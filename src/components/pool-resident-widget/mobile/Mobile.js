import classNames from "classnames";
import { useEffect, useState } from "react";
import { Button, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { formatNumber } from "utils/formats";
import { getResident24hrsVolume } from "utils/residentPool";
import styles from "./Mobile.module.scss";

const LeftSide = ({ residentPool, avgAPEPY, poolIndex }) => {
  const [volume, setVolume] = useState(0);

  const updateVolume = (deposit) => {
    const depositAmount = parseFloat(deposit);
    if (depositAmount !== 0)
      setVolume((prevVol) => {
        return prevVol + depositAmount;
      });
  };

  const calculateVolume = async () => {
    setVolume(0);
    getResident24hrsVolume(poolIndex, updateVolume);
  };

  useEffect(() => {
    calculateVolume();
  }, []);

  return (
    <>
      <div>
        <div>Total Liquidity</div>
        <div>
          <strong>{formatNumber(residentPool.data?.totalLiquidity, 0)}&nbsp;LP</strong>
        </div>
      </div>
      <div className={styles.secondPart}>
        <div>
          Volume: <strong>${volume}</strong>
        </div>
        <div>
          Minimum Deposit: <strong>${formatNumber(residentPool.data?.minDeposit)}</strong>
        </div>
        <div>
          Maximum Deposit: <strong>${formatNumber(residentPool.data?.maxDeposit)}</strong>
        </div>
        <div>
          Average APE PY: <strong>{formatNumber(avgAPEPY.data, 0)}%&nbsp;</strong>
        </div>
      </div>
    </>
  );
};

const RightSide = ({ residentPool, avgAPY, slots, rewardSymbol }) => {
  return (
    <>
      <div className={styles.right}>
        <div>Current reward</div>
        <div>
          <strong>
            {formatNumber(residentPool.data?.currentReward, 2)} {rewardSymbol} / Block
          </strong>
        </div>
      </div>
      <div className={classNames(styles.secondPart, styles.right)}>
        <div>
          Average APY: <strong>{formatNumber(avgAPY.data, 0)}%</strong>
        </div>
        <div>
          Filled Lots:
          <strong className="pool-resident-widget__filled-lots">
            {` ${slots.data?.occupied?.length ?? 0}/${residentPool.data?.maxStakers}`}
          </strong>
        </div>
        <div className={styles.right}>
          Pulse Length:
          <strong>{` ${formatNumber(residentPool.data?.pulseWavelength, 0)} blocks`}</strong>
        </div>
      </div>
    </>
  );
};

const Footer = ({ className, index, styles, tycoon }) => {
  return (
    <>
      <Col xs="12" className="pool-resident-widget__button">
        <NavLink to={`/pool/resident/${index}`} variant="link">
          <Button
            disabled={tycoon}
            className={tycoon ? "tycoon-btn" : styles[`${className}-widget-btn`]}
            variant="secondary">
            Pool Details
          </Button>
        </NavLink>
      </Col>
    </>
  );
};

export { LeftSide, RightSide, Footer };
