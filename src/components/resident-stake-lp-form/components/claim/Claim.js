import classnames from "classnames";
import Steps from "components/steps";
import ToolTip from "components/tool-tip";
import Value from "components/value";
import { MainContext } from "context/context";
import { memo, useContext } from "react";
import { Form } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { formatNumber } from "utils/formats";
import buttonStyles from "../../buttons.module.scss";
import Label from "../label";
import styles from "./Claim.module.scss";

const Claim = ({
  lpBalance,
  deposit,
  selectedSlot,
  handleDeposit,
  handleDepositOnBlur,
  errors,
  burnRate,
  handleBurnRateChange,
  handleBurnRateOnBlur,
  newAPEPY,
  approved,
  requiredApprove,
  setMaxDepositAmount,
  handleSlot,
  showSpinner,
  poolShare
}) => {
  const { isMobile } = useContext(MainContext);
  const showApproveButton = requiredApprove && !errors.depositError && !errors.burnRateError;

  return (
    <Row>
      <Col md="6">
        <Label>Deposit Amount</Label>
        <Row>
          <Col sx="9">
            <Form.Group>
              <Form.Control
                className={classnames(styles.value, styles.valueInput)}
                value={deposit}
                placeholder={`Min. ${formatNumber(selectedSlot.minDeposit, 18)}`}
                onChange={handleDeposit}
                onBlur={handleDepositOnBlur}
                isInvalid={!!errors.depositError}
                inputMode="numeric"
              />
              <Form.Control.Feedback type="invalid">{errors.depositError}</Form.Control.Feedback>
            </Form.Group>
            {/* <InputGroup
              className={classnames(styles.valueInputGroup, !isMobile && styles.highlighted)}
            >
              <FormControl
                className={styles.valueInput}
                value={deposit}
                placeholder={`Min. ${formatNumber(selectedSlot.minDeposit, 18)}`}
                onChange={handleDeposit}
                onBlur={handleDepositOnBlur}
                isInvalid={!!errors.depositError}
                inputMode="numeric"
              />
              <InputGroup.Text className={styles.valueInputUnit}>$LP</InputGroup.Text>
            </InputGroup> */}
          </Col>
          <Col xs="2">
            <Button
              className={buttonStyles.btnOutline}
              variant="outline"
              onClick={() => setMaxDepositAmount()}>
              MAX
            </Button>
          </Col>
        </Row>
        <Label>
          <span>
            Burn Rate <ToolTip text="Burn Rate" />
          </span>
        </Label>
        <Form.Group>
          <Form.Control
            className={classnames(styles.value, styles.valueInput)}
            value={burnRate}
            placeholder={`Min. ${formatNumber(selectedSlot.minBurnRate, 18)}`}
            onChange={handleBurnRateChange}
            onBlur={handleBurnRateOnBlur}
            isInvalid={!!errors.burnRateError}
            inputMode="numeric"
          />
          <Form.Control.Feedback type="invalid">{errors.burnRateError}</Form.Control.Feedback>
        </Form.Group>
        {/* <InputGroup>
          <FormControl
            className={classnames(styles.value, styles.valueInput)}
            value={burnRate}
            placeholder={`Min. ${formatNumber(selectedSlot.minBurnRate, 18)}`}
            onChange={handleBurnRateChange}
            onBlur={handleBurnRateOnBlur}
            isInvalid={!!errors.burnRateError}
            inputMode="numeric"
          />
          <InputGroup.Text className={styles.valueInputUnit}>$LPpp</InputGroup.Text>
        </InputGroup> */}
      </Col>
      <Col md="6">
        <Label>
          <span>
            Pool Share <ToolTip text="Pool Share" />
          </span>
        </Label>
        <Value unit="%">{poolShare ? poolShare : 0}</Value>
        <Label>
          <span>
            Current APE PY <ToolTip text="APEPY = rewardsPerPulse / burnPerPulse" />
          </span>
        </Label>
        <Value className={isMobile && styles.highlighted} unit="%">
          {newAPEPY ? formatNumber(newAPEPY) : 0}
        </Value>
        {showApproveButton && (
          <div className={styles.claimContainer}>
            <div className={styles.claimLeftContainer}>
              <Steps requiredApprove={requiredApprove} approved={approved} />
            </div>
            <div className={styles.claimRightContainer}>
              <Button
                className={buttonStyles.btnSecondary}
                variant="secondary"
                onClick={() => handleSlot(true)}
                disabled={approved}>
                {approved ? "Approved" : "Approve"}
              </Button>
              <Button
                className={buttonStyles.btnSecondary}
                variant="secondary"
                onClick={() => handleSlot()}
                disabled={
                  showSpinner ||
                  !selectedSlot ||
                  selectedSlot.minDeposit > lpBalance?.data ||
                  !approved
                }>
                Claim Lot
              </Button>
            </div>
          </div>
        )}

        {!showApproveButton && (
          <Button
            className={buttonStyles.btnSecondary}
            variant="secondary"
            onClick={() => handleSlot()}
            disabled={
              showSpinner || !selectedSlot || selectedSlot.minDeposit > lpBalance?.data || !approved
            }>
            Claim Lot
          </Button>
        )}
      </Col>
    </Row>
  );
};

export default memo(Claim);
