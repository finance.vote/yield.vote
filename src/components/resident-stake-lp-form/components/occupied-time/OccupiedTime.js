import { memo, useEffect, useState } from "react";

const OccupiedTime = ({ lastUpdatedTimestamp }) => {
  const [time, setTime] = useState(0);

  const timer = () => {
    const now = new Date().getTime();
    const transactionTime = new Date(lastUpdatedTimestamp * 1e3).getTime();

    let seconds = Math.abs((now - transactionTime) / 1000);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);
    let days = Math.floor(hours / 24);
    let years = Math.floor(days / 365);

    seconds = Math.floor(seconds - minutes * 60);
    minutes = Math.floor(minutes - hours * 60);
    hours = Math.floor(hours - days * 24);
    days = Math.floor(days - years * 365);

    var result = `${years > 0 ? years`:` : ""}${days}:${hours}:${minutes}:${seconds}`;
    setTime(result);
  };

  useEffect(() => {
    const timeFn = setInterval(() => timer(), 1000);
    return () => {
      clearInterval(timeFn);
    };
  }, []);

  return (
    <>
      <div>Time occupied: (d:h:m:s)</div>
      <div>{time}</div>
    </>
  );
};

export default memo(OccupiedTime);
