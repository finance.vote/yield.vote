import { memo } from "react";

const TillBurned = ({ getBlocksTillBurned, selectedSlot }) => {
  return (
    <div>
      Blocks till burned:
      {` ${getBlocksTillBurned(selectedSlot?.deposit, selectedSlot?.burnRatePerPulse)}`}
    </div>
  );
};

export default memo(TillBurned);
