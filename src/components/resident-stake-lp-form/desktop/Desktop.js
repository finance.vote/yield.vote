import classnames from "classnames";
import Spinner from "components/indicators/Spinner";
import ToolTip from "components/tool-tip";
import Value from "components/value";
import React from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import ReactSelect from "react-select";
import { formatNumber } from "utils/formats";
import CloseModalButton from "../../close-modal";
import Claim from "../components/claim";
import Label from "../components/label";
import Occupant from "../components/occupant";
import OccupiedTime from "../components/occupied-time";
import Occupy from "../components/occupy";
import TillBurned from "../components/till-burned";
import TotalRewards from "../components/total-reward";
import desktopStyles from "./Desktop.module.scss";

const Desktop = (props) => {
  const {
    lpBalance,
    selectedSlot,
    showSpinner,
    getBlocksTillBurned,
    closeModal,
    slotSelectableOptions,
    handleSlotChange,
    defaultSelectedSlot,
    showImages,
    selectedSlotImage,
    approving,
    symbols
  } = props;

  return (
    <Container className={desktopStyles.stakeContainer} fluid>
      <div className={desktopStyles.header}>
        <div className={desktopStyles.title}>
          Stake LP Tokens
          <span>Claim a Lot to start earning rewards</span>
        </div>
        <CloseModalButton onClick={closeModal} />
      </div>
      <Row>
        <Col sm="12" md="3" className={desktopStyles.leftContent}>
          <div className={desktopStyles.card}>
            <ReactSelect
              className={classnames(desktopStyles.lots, "select")}
              classNamePrefix="select"
              options={slotSelectableOptions}
              onChange={handleSlotChange}
              value={defaultSelectedSlot}
            />
            <div>Select a vacant lot or choose an occupied lot to kick out the owner</div>
            <div className={classnames("mt-5", "mb-4", "text-center")}>
              {showImages && <img className="expand" src={selectedSlotImage} alt="It's a house" />}
            </div>
          </div>
        </Col>

        <Col sm="12" md="9" className={desktopStyles.rightContent}>
          <div className={desktopStyles.card}>
            <Row>
              <Col sm="4" className={desktopStyles.lotTitle}>
                Lot {selectedSlot?.index}
                {selectedSlot?.vacant && <React.Fragment> - Vacant</React.Fragment>}
                {selectedSlot && !selectedSlot?.vacant && (
                  <React.Fragment> - Occupied</React.Fragment>
                )}
              </Col>
              {selectedSlot?.vacant && lpBalance?.data && (
                <Col sm="8" className={classnames(desktopStyles.tokenBalance, "text-right")}>
                  SLP Token Balance: <span>{formatNumber(lpBalance?.data)} SLP</span>
                </Col>
              )}
            </Row>
            {selectedSlot?.vacant && <Claim {...props} />}
            {selectedSlot && !selectedSlot?.vacant && (
              <Row>
                <Col md="6">
                  <Occupant selectedSlot={selectedSlot} />
                  <Label>Current Deposit</Label>
                  <Value unit="LP">{formatNumber(selectedSlot?.deposit, 18)}</Value>
                  <Label>Current Burn Rate</Label>
                  <Value unit="LP / Pulse">
                    {formatNumber(selectedSlot?.burnRatePerPulse, 18)}
                  </Value>
                  <Label>
                    Current APEPY
                    <ToolTip text="APEPY = rewardsPerPulse / burnPerPulse" />
                  </Label>
                  <Value unit="%">{formatNumber(selectedSlot?.apepy)}</Value>
                  <div className={desktopStyles.footerStats}>
                    <TillBurned
                      getBlocksTillBurned={getBlocksTillBurned}
                      selectedSlot={selectedSlot}
                    />
                    <TotalRewards
                      rewards={selectedSlot.rewardsSum}
                      symbol={symbols?.reward1TokenSymbol}
                    />
                    <OccupiedTime lastUpdatedTimestamp={selectedSlot.lastUpdatedTime} />
                  </div>
                </Col>
                <Col md="6">
                  <Occupy {...props} />
                </Col>
              </Row>
            )}
          </div>
        </Col>
      </Row>
      {(showSpinner || approving) && <Spinner overlay={true} />}
    </Container>
  );
};

export { Desktop };
