import HouseImage from "assets/house-01.svg";
import * as houses from "assets/houses";
import Spinner from "components/indicators/Spinner";
import { MainContext } from "context/context";
import { ethInstance, toBN, toWei } from "evm-chain-scripts";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useMemo, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import ReactSelect from "react-select";
import { toast } from "react-toastify";
import { formatNumber } from "utils/formats";
import { getChainSettings, POOL_TYPES, TRANSACTION_STATUS } from "utils/poolHelper";
import { useResidentDistanceToPulseEnd } from "../../utils/queries";
import CloseModalButton from "../close-modal";
import "./ResidentWithdrawForm.scss";

const WithdrawRewardsComponent = ({
  selectedSlot,
  symbols,
  showSpinner,
  pendingTransaction,
  slotSumbit
}) => {
  const isAdditionalRewards = selectedSlot ? toBN(toWei(selectedSlot.rewards2)) > 0 : false;
  const reward1TokenSymbol =
    symbols.reward1TokenSymbol?.[0] !== "$"
      ? `$${symbols.reward1TokenSymbol}`
      : symbols.reward1TokenSymbol;

  const reward2TokenSymbol =
    symbols.reward2TokenSymbol?.[0] !== "$"
      ? `$${symbols.reward2TokenSymbol}`
      : symbols.reward2TokenSymbol;

  return (
    <>
      <div className="resident-withdraw__label">Rewards available</div>
      <div className="resident-withdraw__value highlighted">
        <div>{`${formatNumber(selectedSlot?.rewards1)} ${
          isAdditionalRewards ? " + " + formatNumber(selectedSlot?.rewards2) : ""
        }`}</div>
        <div className="resident-withdraw__value-currency">
          {`${reward1TokenSymbol} ${isAdditionalRewards ? ` + ` + reward2TokenSymbol : ""}`}
        </div>
      </div>
      <Button
        variant="secondary"
        onClick={() => slotSumbit("withdraw")}
        disabled={showSpinner || !selectedSlot || pendingTransaction}>
        Withdraw Rewards
      </Button>
    </>
  );
};
function ResidentWithdrawForm({
  poolIndex,
  closeModal,
  slot,
  slotIds,
  slotAction,
  showImages,
  symbols,
  lastAccountTransactions,
  setLastAccountTransactions
}) {
  const { address, isMobile } = useContext(MainContext);
  const [showSpinner, setShowSpinner] = useState(false);
  const [selectedSlot, setSelectedSlot] = useState(null);
  const [selectedSlotImage, setSelectedSlotImage] = useState(HouseImage);
  const [defaultSelectedSlot, setDefaultSelectedSlot] = useState(null);
  const [pendingTransaction, setPendingTransaction] = useState(false);
  const [lastTransactionTime, setLastTransactionTime] = useState(0);
  const blocksTillPulseEnd = useResidentDistanceToPulseEnd(poolIndex);

  const slotSelectableOptions = useMemo(
    () =>
      slotIds
        ? slotIds.map((slot, i) => ({
            value: i,
            label: `Lot ${slot.index}`
          }))
        : [],
    [slotIds]
  );

  const slotSumbit = async (actionType) => {
    setShowSpinner(true);
    try {
      const result = await slotAction(poolIndex, selectedSlot.index, actionType);

      if (result?.status === TRANSACTION_STATUS.CONFIRMED) {
        toast.success(`Successful ${actionType === "vacate" ? "vacate" : "payout"}!`, {
          position: "top-center"
        });
      } else {
        toast.warn(`Fail payout`, { position: "top-center" });
      }
    } catch (error) {
      console.log(error);
      toast.warn(`Fail payout`, { position: "top-center" });
    } finally {
      setShowSpinner(false);
      closeModal();
    }
  };

  function handleSlotChange({ value }) {
    setDefaultSelectedSlot(slotSelectableOptions[value]);
    setSelectedSlot(slotIds[value]);
  }

  const getSlotTransaction = async (selectedSlot) => {
    const localData = localStorage.getItem("pendingTransactions");
    const pendingTransactions = localData ? JSON.parse(localData) : [];

    const slot = selectedSlot ?? slotIds[0];
    let lastTransactionTime = 0;
    let setPendingToConfirm = [];

    let pendingSlotTransaction = pendingTransactions.find(
      (x) => x.poolId === poolIndex && x.slotId === slot.index && x.poolType === POOL_TYPES.RESIDENT
    );

    if (pendingSlotTransaction) {
      try {
        const transaction = await ethInstance.getTransaction(pendingSlotTransaction.hash);

        if (!transaction) {
          setPendingTransaction(true);
        } else {
          setPendingTransaction(false);
          const index = pendingTransactions.indexOf(pendingSlotTransaction);

          pendingTransactions[index].status = transaction.status;
          pendingTransactions[index].blockNumber = transaction.blockNumber;
          pendingTransactions[index].poolType = POOL_TYPES.RESIDENT;

          setPendingToConfirm = [pendingTransactions[index]];

          // add new transaction after pending status to all account transaction cache
          setLastAccountTransactions([...lastAccountTransactions, ...setPendingToConfirm]);

          if (index > -1) pendingTransactions.splice(index, 1);

          localStorage.setItem(
            "pendingTransactions",
            JSON.stringify(pendingTransactions.length > 0 ? pendingTransactions : [])
          );

          pendingSlotTransaction = null;
        }
      } catch (error) {
        console.log("get transaction: ", error);
      }
    }

    if (!pendingSlotTransaction) {
      if (!address) return;
      const currentBlock = (await ethInstance.getBlock("latest")).number;
      const chainId = ethInstance.getChainId();
      const { avgBlockTime } = getChainSettings(chainId);

      // about 5 last minutes
      const fromBlock = Math.floor(currentBlock - (0.08 * 60 * 60) / avgBlockTime);

      // search in last account transactions and new one just after pending
      const slotTranCache = [...setPendingToConfirm, ...lastAccountTransactions].find(
        (x) =>
          x.poolId === poolIndex &&
          x.slotId === slot.index &&
          x.status === TRANSACTION_STATUS.CONFIRMED &&
          x.poolType === POOL_TYPES.RESIDENT
      );

      if (slotTranCache) {
        // get time from cache
        const time = await ethInstance.getBlock(slotTranCache.blockNumber);
        lastTransactionTime = (Date.now() / 1e3 - time.timestamp) / 60;
      } else {
        // get all updated transactions by wallet address
        const lastTransactions = await ethInstance.getWalletTransactionsHistory(
          address,
          "function updateSlot(uint256 poolId, uint256 slotId)",
          ethInstance.getChainId() === 1 ? "homestead" : "ropsten",
          fromBlock,
          currentBlock
        );

        let parsedTransactions = lastTransactions
          .filter((x) => x)
          .map((x) => {
            return {
              blockNumber: x.blockNumber,
              status: x.status,
              poolId: parseInt(x.poolId?.toString()),
              slotId: parseInt(x.slotId?.toString()),
              poolType: POOL_TYPES.RESIDENT
            };
          });

        if (parsedTransactions.length > 0) {
          // get last transaction on slot
          const slotTransaction = parsedTransactions
            .filter(
              (x) =>
                x.poolId === poolIndex &&
                x.slotId === slot.index &&
                x.status === TRANSACTION_STATUS.CONFIRMED
            )
            .sort((x) => x.blockNumber);

          // set transactions to cache
          setLastAccountTransactions([...parsedTransactions]);

          if (slotTransaction.length > 0) {
            const lastTransaction = slotTransaction[slotTransaction.length - 1];
            const time = await ethInstance.getBlock(lastTransaction.blockNumber);
            lastTransactionTime = (Date.now() / 1e3 - time.timestamp) / 60;
          }
        }
      }

      // show time only below 5 minutes
      setLastTransactionTime(lastTransactionTime <= 5 ? lastTransactionTime : 0);
    }
  };

  useEffect(() => {
    if (slotSelectableOptions && !selectedSlot) {
      if (slot === undefined) {
        setDefaultSelectedSlot(slotSelectableOptions[0]);
        setSelectedSlot(slotIds[0]);
      } else {
        const slotIndex = slotIds.findIndex((slotData) => slotData.index === slot);

        if (slotIndex > -1) {
          setDefaultSelectedSlot(slotSelectableOptions[slotIndex]);
          setSelectedSlot(slotIds[slotIndex]);
        }
      }
    }
  }, [slotSelectableOptions]);

  useEffect(() => {
    setPendingTransaction(false);
    setLastTransactionTime(0);

    let houseImage = HouseImage;
    const imageName = `Lot${selectedSlot?.index}${selectedSlot?.vacant ? "Vacant" : "Occupied"}`;

    if (houses[imageName]) houseImage = houses[imageName].default;
    setSelectedSlotImage(houseImage);

    if (!selectedSlot) return;

    getSlotTransaction(selectedSlot);
  }, [selectedSlot]);

  return (
    <Container className="resident-withdraw" fluid>
      <div className="resident-withdraw-header">
        <div className="resident-withdraw-header__title">
          Withdraw or Vacate
          <span>
            Withdraw your rewards or vacate your lot to leave with your deposit and rewards
          </span>
        </div>
        <CloseModalButton onClick={closeModal} />
      </div>
      <Form>
        <Row>
          <Col sm="12" md="3" className="resident-withdraw__left-content">
            <div className="resident-withdraw__card">
              <ReactSelect
                className="resident-withdraw__lots select"
                classNamePrefix="select"
                options={slotSelectableOptions}
                onChange={handleSlotChange}
                value={defaultSelectedSlot}
              />
              <div>Select one of your lots to withdraw or vacate</div>
              <div className="mt-5 mb-4 text-center">
                {showImages && (
                  <img className="expand" src={selectedSlotImage} alt="It's a house" />
                )}
              </div>
            </div>
          </Col>
          <Col sm="12" md="9" className="resident-withdraw__right-content">
            <div className="resident-withdraw__card">
              <Row>
                {!isMobile ? (
                  <>
                    <Col sm="4" className="resident-withdraw__lot-title">
                      Lot {selectedSlot?.index}
                    </Col>
                    <Col sm="8" className="text-right">
                      Blocks till end of pulse: {blocksTillPulseEnd.data}
                    </Col>
                  </>
                ) : (
                  <div className="resident-withdraw__lot-mobile-title">Lot details:</div>
                )}
              </Row>
              <Row>
                <Col md="6">
                  <div className="resident-withdraw__label">Initial Deposit</div>
                  <div className="resident-withdraw__value">
                    <div>{formatNumber(selectedSlot?.initialDeposit)}</div>
                    <div className="resident-withdraw__value-currency">$LP</div>
                  </div>
                  {!isMobile && (
                    <WithdrawRewardsComponent
                      selectedSlot={selectedSlot}
                      symbols={symbols}
                      showSpinner={showSpinner}
                      pendingTransaction={pendingTransaction}
                      slotSumbit={slotSumbit}
                    />
                  )}
                </Col>
                <Col md="6">
                  <div className="resident-withdraw__label">$LP burned</div>
                  <div className="resident-withdraw__value">
                    <div>
                      {selectedSlot
                        ? formatNumber(selectedSlot.initialDeposit - selectedSlot.deposit)
                        : 0}
                    </div>
                    <div className="resident-withdraw__value-currency">$LP</div>
                  </div>
                  <div className="resident-withdraw__label">$LP left to withdraw</div>
                  <div className="resident-withdraw__value">
                    <div>{formatNumber(selectedSlot?.deposit)}</div>
                    <div className="resident-withdraw__value-currency">$LP</div>
                  </div>
                  {isMobile && (
                    <WithdrawRewardsComponent
                      selectedSlot={selectedSlot}
                      symbols={symbols}
                      showSpinner={showSpinner}
                      pendingTransaction={pendingTransaction}
                      slotSumbit={slotSumbit}
                    />
                  )}
                  <Button
                    variant="outline-secondary"
                    onClick={() => slotSumbit("vacate")}
                    disabled={showSpinner || !selectedSlot}>
                    Vacate Lot
                  </Button>
                </Col>
              </Row>
              {!pendingTransaction && lastTransactionTime > 0 && (
                <div>{`Your last withdrawal time: < ${
                  lastTransactionTime < 1 ? 1 : lastTransactionTime.toFixed(0)
                } min. ago`}</div>
              )}
              {pendingTransaction && <div>{`Transaction is pending...`}</div>}
            </div>
          </Col>
        </Row>
      </Form>
      {showSpinner && <Spinner overlay={true} />}
    </Container>
  );
}

ResidentWithdrawForm.propTypes = {
  poolIndex: PropTypes.number,
  position: PropTypes.object
};

export default memo(ResidentWithdrawForm);
