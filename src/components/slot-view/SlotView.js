import { useState } from "react";
import { Statistic, Card, Button, InputNumber } from "antd";
import { useResidentSlot } from "utils/queries";
import { claimSlot, updateSlot } from "utils/liquidity";

function SlotView({ contractName, slotId }) {
  const slot = useResidentSlot(contractName, slotId);

  const [burnRate, setBurnRate] = useState(0);
  const [deposit, setDeposit] = useState(0);

  const betterDeal =
    (slot.data?.burnRate < burnRate && slot.data?.deposit < deposit) || slot.data?.deposit == 0;

  return (
    <div>
      <Card title={`Slot # ${slotId}`}>
        <Statistic title="Owner" value={slot.data?.owner} />
        <Statistic title="Burn Rate" value={slot.data?.burnRate} />
        <Statistic title="Rewards" value={slot.data?.rewards} />
        <Statistic title="Deposit" value={slot.data?.deposit} />
        <Card title={"New burn rate"}>
          <InputNumber
            min={slot.data?.deposit == 0 ? 0 : slot.data?.burnRate}
            defaultValue={slot.data?.burnRate}
            onChange={setBurnRate}
          />
        </Card>
        <Card title={"New deposit"}>
          <InputNumber
            min={slot.data?.deposit}
            defaultValue={slot.data?.deposit}
            onChange={setDeposit}
          />
        </Card>
        <Button
          disabled={!betterDeal}
          type="primary"
          onClick={() => claimSlot(contractName, slotId, burnRate, deposit)}
        >
          Claim Slot
        </Button>
        <Button type="primary" onClick={() => updateSlot(contractName, slotId)}>
          Update Slot
        </Button>
      </Card>
    </div>
  );
}

export default SlotView;
