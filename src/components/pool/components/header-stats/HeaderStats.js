import { formatNumber } from "utils/formats";
import ToolTip from "components/tool-tip";
import { memo } from "react";

const HeaderStats = ({ residentPool, avgAPEPY, avgAPY, className }) => {
  return (
    <div className={className}>
      <div>
        Total Liquidity: <strong>{residentPool.data?.totalLiquidity}&nbsp;SLP</strong>
      </div>
      <div>
        Average APE PY: <strong>{formatNumber(avgAPEPY?.data, 0)}%&nbsp;</strong>
        <ToolTip text="APEPY = rewardsPerPulse / burnPerPulse" />
      </div>
      <div>
        Average APY: <strong>{formatNumber(avgAPY.data, 0)}%</strong>
      </div>
    </div>
  );
};

export default memo(HeaderStats);
