import Rewards from "components/rewards";
import { memo } from "react";
import styles from "./Graph.module.scss";

const Graph = ({ poolIndex, dataForGraph, partnerStyles, residentPool, reward1Symbol }) => {
  return (
    <div className={styles.graph}>
      <div className={styles.title}>Rewards</div>
      <Rewards
        poolIndex={poolIndex}
        datas={dataForGraph}
        styles={partnerStyles}
        isPoolLoading={
          residentPool.isLoading ||
          residentPool.isFetching ||
          reward1Symbol.length === 0 ||
          !partnerStyles
        }
      />
    </div>
  );
};

export default memo(Graph);
