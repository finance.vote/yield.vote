import { memo } from "react";
import styles from "./SummaryStats.module.scss";

const SummaryStats = ({ myRewards, mySlots, reward1Symbol, isMobile = false }) => {
  const firstLabel = isMobile ? "Your own" : "Lots";
  return (
    <div className={styles.summaryStats}>
      <div>
        <div className={styles.label}>{firstLabel}</div>
        <div className={styles.value}>{mySlots.length ?? 0}</div>
      </div>
      <div>
        <div className={styles.label}>Total Rewards</div>
        <div className={styles.value}>
          {parseFloat(myRewards).toFixed(4) +
            (reward1Symbol?.[0] !== "$" ? ` $` : " ") +
            `${reward1Symbol}`}
        </div>
      </div>
    </div>
  );
};

export default memo(SummaryStats);
