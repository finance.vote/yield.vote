const { memo } = require("react");
import { formatNumber } from "utils/formats";
import styles from "./PoolDetails.module.scss";

const PoolDetails = ({ volume, residentPool, slotIds }) => {
  return (
    <div className={styles.details}>
      <div className={styles.title}>Pool details:</div>
      <div>
        Volume: <strong>{formatNumber(volume, 3)} SLP</strong> (24hrs)
      </div>
      <div>
        Minimum Deposit <strong>{formatNumber(residentPool.data?.minDeposit, 0)} SLP</strong>
      </div>
      <div>
        Maximum Deposit: <strong>{formatNumber(residentPool.data?.maxDeposit, 0)} SLP</strong>
      </div>
      <div>
        Lots Filled:
        <strong>{` ${slotIds.occupied?.length}/${residentPool.data?.maxStakers}`}</strong>
      </div>
      <div>
        Current Pulse Length:
        <strong>{` ${formatNumber(residentPool.data?.pulseWavelength, 0)} blocks`}</strong>
        {` (~${formatNumber(residentPool.data?.pulseLengthByWeeks, 0)}`}&nbsp;
        {`${residentPool.data?.pulseLengthByWeeks > 1.5 ? "weeks" : "week"})`}
      </div>
      <div>
        Slot Rewards / Pulse: <strong>{formatNumber(residentPool.data?.pulseIntegral, 0)}</strong>
      </div>
    </div>
  );
};

export default memo(PoolDetails);
