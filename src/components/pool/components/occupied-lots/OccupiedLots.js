import { memo } from "react";
import PropTypes from "prop-types";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Nav from "react-bootstrap/Nav";
import Tab from "react-bootstrap/Tab";
import LotRows from "./lot-rows";
import "./OccupiedLots.scss";

function OccupiedLots({ mySlots, occupied, claimSlot, withdrawSlot, symbols, isMobile = false }) {
  return (
    <Tab.Container defaultActiveKey="occupied-lots">
      <Row className="occupied-lots">
        <Col>
          <Row>
            <Col className="occupied-lots__title">
              <Nav>
                <Nav.Item>
                  <Nav.Link eventKey="my-lots">
                    My Lots <strong>{mySlots.length ?? 0}</strong>
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="occupied-lots">Occupied Lots</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
          </Row>
          <Row>
            <Col>
              <Tab.Content>
                <Tab.Pane eventKey="my-lots">
                  <LotRows
                    slots={mySlots}
                    claimSlot={claimSlot}
                    withdrawSlot={withdrawSlot}
                    symbols={symbols}
                    isMyLots={true}
                    isMobile={isMobile}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="occupied-lots">
                  <LotRows
                    slots={occupied}
                    claimSlot={claimSlot}
                    withdrawSlot={withdrawSlot}
                    symbols={symbols}
                    isMyLots={false}
                    isMobile={isMobile}
                  />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Col>
      </Row>
    </Tab.Container>
  );
}

OccupiedLots.propTypes = {
  slotIds: PropTypes.object
};

export default memo(OccupiedLots);
