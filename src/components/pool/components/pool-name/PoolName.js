import { memo } from "react";

const PoolName = ({ className, poolName }) => {
  return <div className={className}>{poolName}</div>;
};

export default memo(PoolName);
