import Spinner from "components/indicators/Spinner";
import ResidentStakeLPForm from "components/resident-stake-lp-form";
import ResidentWithdrawForm from "components/resident-withdraw-form";
import { Modal } from "react-bootstrap";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import PoolName from "../../pool-basic/components/pool-name";
import Actions from "../components/actions";
import PoolDetails from "../components/details";
import Graph from "../components/graph";
import HeaderStats from "../components/header-stats";
import OccupiedLots from "../components/occupied-lots";
import SummaryStats from "../components/summary-stats/SummaryStats";
import desktopStyle from "./Desktop.module.scss";

const Desktop = ({
  residentPool,
  avgAPEPY,
  avgAPY,
  isFuturePool,
  address,
  handleShowStakeDialog,
  handleShowWithdrawDialog,
  slotIds,
  residentSlots,
  volume,
  mySlots,
  myRewards,
  reward1Symbol,
  poolIndex,
  dataForGraph,
  partnerStyles,
  poolSymbols,
  showStakeDialog,
  handleCloseStakeDialog,
  claimSlot,
  approveSlot,
  userLPBalance,
  selectedLot,
  approving,
  accountAllowance,
  showImages,
  showWithdrawDialog,
  handleCloseWithdrawDialog,
  slotAction,
  lastAccountTransactions,
  setLastAccountTransactions
}) => {
  return (
    <Container fluid>
      {residentPool.isLoading && <Spinner overlay={true} />}
      <Row>
        <Col md="8" xs="12" className={desktopStyle.total}>
          <PoolName className={desktopStyle.poolName} poolName={residentPool.data?.name} />
          <HeaderStats
            className={desktopStyle.headerStats}
            avgAPEPY={avgAPEPY}
            avgAPY={avgAPY}
            residentPool={residentPool}
          />
        </Col>
        <Col md="4" xs="12" className={desktopStyle.actions}>
          <Actions
            isFuturePool={isFuturePool}
            address={address}
            handleShowStakeDialog={handleShowStakeDialog}
            handleShowWithdrawDialog={handleShowWithdrawDialog}
            slotIds={slotIds}
            residentSlots={residentSlots}
          />
        </Col>
      </Row>
      <Row className={desktopStyle.equalHeights}>
        <Col xs="12" md="5">
          <div className={desktopStyle.box}>
            <PoolDetails volume={volume} residentPool={residentPool} slotIds={slotIds} />
          </div>
          <div className={desktopStyle.box}>
            <SummaryStats myRewards={myRewards} mySlots={mySlots} reward1Symbol={reward1Symbol} />
          </div>
        </Col>
        <Col xs="12" md="7">
          <Graph
            poolIndex={poolIndex}
            dataForGraph={dataForGraph}
            partnerStyles={partnerStyles}
            residentPool={residentPool}
            reward1Symbol={reward1Symbol}
          />
        </Col>
      </Row>
      <OccupiedLots
        mySlots={mySlots}
        occupied={slotIds?.occupied}
        claimSlot={handleShowStakeDialog}
        withdrawSlot={handleShowWithdrawDialog}
        symbols={poolSymbols}
      />

      <Modal
        className="stake-lp-modal"
        show={showStakeDialog}
        onHide={handleCloseStakeDialog}
        centered>
        <Modal.Body>
          <ResidentStakeLPForm
            tokenAddress={residentPool.data?.liquidityToken}
            claimSlot={claimSlot}
            approve={approveSlot}
            closeModal={handleCloseStakeDialog}
            poolIndex={poolIndex ? parseInt(poolIndex) : null}
            lpBalance={userLPBalance ? parseFloat(userLPBalance.data).toFixed(2) : "0"}
            slot={selectedLot}
            slotIds={slotIds}
            residentPool={residentPool}
            approving={approving}
            accountAllowance={accountAllowance.data ?? 0}
            showImages={showImages}
            symbols={poolSymbols}
          />
        </Modal.Body>
      </Modal>
      <Modal
        className="resident-withdraw-modal"
        show={showWithdrawDialog}
        onHide={handleCloseWithdrawDialog}
        centered>
        <Modal.Body>
          <ResidentWithdrawForm
            slotAction={slotAction}
            poolIndex={poolIndex ? parseInt(poolIndex) : null}
            closeModal={handleCloseWithdrawDialog}
            slot={selectedLot}
            slotIds={mySlots}
            symbols={poolSymbols}
            showImages={showImages}
            lastAccountTransactions={lastAccountTransactions}
            setLastAccountTransactions={setLastAccountTransactions}
          />
        </Modal.Body>
      </Modal>
    </Container>
  );
};

export { Desktop };
