import FinanceVoteImage from "assets/FinanceVoteLogo.svg";
import ITrustLogoImage from "assets/ITrustLogo.svg";
import Spinner from "components/indicators/Spinner";
import Table from "components/table";
import { memo, useEffect, useState } from "react";
import { formatNumber } from "utils/formats";
import { getPartnersDeposit } from "utils/helper";
import { useGetPoolsAll } from "utils/queries/useAllPools";

const choosePartnerImage = (partnerName) => {
  switch (partnerName) {
    case "finance":
      return FinanceVoteImage;
    case "itrust":
      return ITrustLogoImage;
  }
};

const link = (cell, row) => {
  return (
    <a href={"/#/pool/resident/" + cell} rel="noreferrer">
      {row.name}
    </a>
  );
};

function ResidentPoolRows() {
  const pools = useGetPoolsAll("resident");
  const [datas, setDatas] = useState([]);
  const [loadingData, setLoadingData] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        setLoadingData(true);
        if (pools.data) {
          const { partners, deposits } = await getPartnersDeposit();

          for (let i = 0; i < pools.data?.length; i++) {
            const pool = pools.data[i];
            const key = deposits[pool.liquidityToken.toLowerCase()];
            if (key && key !== "") partners[key].pools.push(pool);
          }

          let data =
            Object.values(partners).length > 0
              ? Object.values(partners).map((x) => {
                  return {
                    ...x,
                    img: choosePartnerImage(x.key)
                  };
                })
              : [];
          setDatas(data);
        }
      } catch (error) {
        console.error("Resident pool rows loading fail: " + error);
      } finally {
        setLoadingData(false);
      }
    })();
  }, [pools.data]);

  const columns = [
    {
      accessor: "index",
      Header: "Index ⇅",
      Cell: ({ value, row }) => link(value, row)
    },
    {
      accessor: "totalLiquidity",
      Header: "Total Liquidity, LP ⇅",
      Cell: ({ value }) => formatNumber(value, 0)
    },
    {
      accessor: "currentReward",
      Header: "Current Reward, FVT / Block ⇅",
      Cell: ({ value }) => formatNumber(value, 2)
    },
    {
      accessor: "pulseWavelength",
      Header: "Pulse Length ⇅",
      Cell: ({ value }) => formatNumber(value, 0)
    },
    {
      accessor: "minDeposit",
      Header: "Minimum Deposit, LP ⇅",
      Cell: ({ value }) => formatNumber(value)
    },
    {
      accessor: "maxDeposit",
      Header: "Maximum Deposit, LP ⇅",
      Cell: ({ value }) => formatNumber(value)
    },
    {
      accessor: "totalStaked",
      Header: "Filled Lots ⇅"
    }
  ];

  return (
    <div className="all-pools">
      {(loadingData || pools.isLoading) && <Spinner />}
      {datas.map((x, index) => {
        return (
          <div className="all-pools__section" key={`all-pools__section-${index}`}>
            <div className="all-pools__section__header">
              <a href={x.url}>
                <img src={x.img} alt={x.name} />
              </a>
            </div>
            <Table data={x.pools} columns={columns} />
          </div>
        );
      })}
    </div>
  );
}

export default memo(ResidentPoolRows);
