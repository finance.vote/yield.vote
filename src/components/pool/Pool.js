import { MainContext } from "context/context";
import { getAddress } from "ethers/lib/utils";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { generateEventsSubscription } from "utils/eventSub";
import { POOL_TYPES, TRANSACTION_STATUS, contracts } from "utils/poolHelper";
import { useCurrentBlock, useResidentPercentageStats, useResidentSlots } from "utils/queries";
import { use24HrsVolume } from "utils/queries/useDayVolume";
import { useFactoryAccountBalance } from "utils/queries/useFactoryAccountBalance";
import { useGetPoolGlobals } from "utils/queries/useGetGlobals";
import { usePoolAllowance } from "utils/queries/usePoolAllowance";
import {
  approveResidentSlot,
  claimResidentSlot,
  updateResidentSlot,
  withdrawResidentFromSlot
} from "utils/residentPool";
import { Desktop as DesktopComponent } from "./desktop";
import { Mobile as MobileComponent } from "./mobile";

function Pool({ poolIndex, partnerStyles }) {
  const { address, currentNetwork, isMobile } = useContext(MainContext);
  const residentPool = useGetPoolGlobals("resident", poolIndex, currentNetwork);
  const residentSlots = useResidentSlots(poolIndex, currentNetwork);
  const currentBlock = useCurrentBlock();
  const poolSymbols = residentPool.data?.symbols ?? {};
  const reward1Symbol = poolSymbols.reward1TokenSymbol ?? "";
  const contract = contracts.Resident;

  const dataForGraph = {
    waveLength: residentPool.data?.pulseWavelength,
    pulseAmplitudeWei: residentPool.data?.pulseAmplitudeWei,
    pulseStartBlock: residentPool.data?.pulseStartBlock,
    symbol: (reward1Symbol?.[0] !== "$" ? ` $` : " ") + `${reward1Symbol}`
  };

  const accountAllowance = usePoolAllowance("resident", residentPool.data?.liquidityToken);
  const slotIds = residentSlots.data ?? {};
  const {
    data: { avgAPEPY, avgAPY }
  } = useResidentPercentageStats(slotIds, currentNetwork);
  const { data: volume, refetch: refetchVolume } = use24HrsVolume(
    "resident",
    poolIndex,
    currentNetwork
  );
  const isFuturePool = residentPool.data?.pulseStartBlock > currentBlock.data;
  const [approving, setApproving] = useState(false);
  const [lastAccountTransactions, setLastAccountTransactions] = useState([]);
  const showImages = true;
  const mySlots = slotIds?.occupied
    ? slotIds.occupied.filter((x) => x.owner.toLowerCase() === address?.toLowerCase())
    : [];

  const myRewards = mySlots.reduce((a, b) => a + b.rewardsSum, 0);

  const loadData = async () => {
    residentSlots.refetch();
    refetchVolume();
  };

  useEffect(() => {
    loadData();
  }, [poolIndex, currentNetwork]);

  useEffect(() => {
    if (!contract) return;

    const { address: contractAddress } = contract.networks[currentNetwork];

    if (!contractAddress) return;

    generateEventsSubscription(
      ["SlotChangedHands"],
      currentNetwork,
      getAddress(contractAddress),
      loadData
    );
  }, []);

  const [selectedLot, setSelectedLot] = useState(undefined);
  const [showStakeDialog, setShowStakeDialog] = useState(false);
  const handleCloseStakeDialog = () => setShowStakeDialog(false);
  const handleShowStakeDialog = (slot) => {
    setSelectedLot(slot);
    setShowStakeDialog(true);
  };

  const [showWithdrawDialog, setShowWithdrawDialog] = useState(false);
  const handleCloseWithdrawDialog = () => setShowWithdrawDialog(false);
  const handleShowWithdrawDialog = (slot) => {
    setSelectedLot(slot);
    setShowWithdrawDialog(true);
  };

  const userLPBalance = useFactoryAccountBalance("resident", residentPool.data?.liquidityToken);
  const approveSlot = async (depo, setApproved) => {
    try {
      const setApprove = (result) => {
        setApproved(result.status);
        if (result.status === TRANSACTION_STATUS.CONFIRMED)
          toast.success(`Transaction approved!`, { position: "top-center" });
        else toast.warn(`Transaction unapproved`, { position: "top-center" });
        setApproving(false);
      };

      setApproving(true);
      const approveTransaction = await approveResidentSlot(depo, residentPool.data?.liquidityToken);

      const result = await approveTransaction.wait();
      setApprove(result);
    } catch (error) {
      toast.warn(`Transaction unapproved`, { position: "top-center" });
      console.error("claim resident slot failure: " + error.message);
    } finally {
      setApproving(false);
    }
  };

  const claimSlot = async (poolIndex, index, burn, depo) => {
    try {
      const transaction = await claimResidentSlot(
        poolIndex,
        index,
        burn,
        depo,
        residentPool.data?.liquidityToken
      );

      const receipt = await transaction.wait();
      if (receipt?.status === TRANSACTION_STATUS.CONFIRMED) loadData();

      return receipt;
    } catch (error) {
      console.error("claim resident slot failure: " + error.message);
    }
  };

  const slotAction = async (poolIndex, index, type) => {
    try {
      let transaction;
      if (type === "withdraw") transaction = await updateResidentSlot(poolIndex, index);
      if (type === "vacate") transaction = await withdrawResidentFromSlot(poolIndex, index);

      const local = localStorage.getItem("pendingTransactions");
      const existingTransactions = local ? JSON.parse(local) : [];
      const newTransactions = [
        ...existingTransactions,
        {
          poolId: poolIndex,
          slotId: index,
          hash: transaction.hash,
          poolType: POOL_TYPES.RESIDENT
        }
      ];

      localStorage.setItem("pendingTransactions", JSON.stringify(newTransactions));

      const receipt = await transaction.wait();

      if (receipt?.status === TRANSACTION_STATUS.CONFIRMED) {
        loadData();
      }

      return receipt;
    } catch (error) {
      console.error("resident action slot failure: " + error);
    }
  };

  const componentProps = {
    residentPool,
    avgAPEPY,
    avgAPY,
    isFuturePool,
    address,
    handleShowStakeDialog,
    handleShowWithdrawDialog,
    slotIds,
    residentSlots,
    volume,
    mySlots,
    myRewards,
    reward1Symbol,
    poolIndex,
    dataForGraph,
    partnerStyles,
    poolSymbols,
    showStakeDialog,
    handleCloseStakeDialog,
    claimSlot,
    approveSlot,
    userLPBalance,
    selectedLot,
    showImages,
    approving,
    accountAllowance,
    showWithdrawDialog,
    handleCloseWithdrawDialog,
    slotAction,
    lastAccountTransactions,
    setLastAccountTransactions
  };

  const ResidentComponent = isMobile ? MobileComponent : DesktopComponent;

  return (
    <>
      <ResidentComponent {...componentProps} />
    </>
  );
}

Pool.propTypes = {
  name: PropTypes.string
};

export default memo(Pool);
