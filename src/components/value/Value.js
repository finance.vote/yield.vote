import { memo } from "react";
import styles from "./Value.module.scss";
import classnames from "classnames";

const Value = ({ className, children, unit }) => {
  return (
    <div className={classnames(className, styles.value)}>
      <div>{children}</div>
      <div className={styles.unit}>{unit}</div>
    </div>
  );
};

export default memo(Value);
