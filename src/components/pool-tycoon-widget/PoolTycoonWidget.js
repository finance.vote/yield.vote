import { memo } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { NavLink } from "react-router-dom";
import "./PoolTycoonWidget.scss";

//        <Row>
//          <Col xs="6">
//            <div>Total Liquidity</div>
//            <div><strong>$</strong></div>
//            <div>Current reward</div>
//            <div><strong>$FVT</strong></div>
//          </Col>
//          <Col xs="6">
//            <div>Volume: <strong>${globals.maxDeposit}</strong> (24hrs)</div>
//            <div>Minimum Deposit: <strong>$FVT</strong></div>
//            <div>Maximum Deposit: <strong>$FVT</strong></div>
//            <div>Average APE PY: </div>
//            <div>Filled Lots: </div>
//            <div>1st Pulse Length: </div>
//          </Col>
//        </Row>

function PoolTycoonWidget({ image }) {
  return (
    <Col sm="12" md="6" xl="4" className="mb-3 widget">
      <div className="pool-tycoon-widget">
        <div className="pool-tycoon-widget__title">FVT Tycoon Pool</div>
        <Row className="pool-tycoon-widget__data" />
        <Row className="pool-tycoon-widget__footer">
          <Col xs="6">
            <div className="image-container">
              <img src={image} alt="tycoon-pool" />
            </div>
          </Col>
          <Col xs="6" className="pool-tycoon-widget__button">
            <NavLink to="/pool/tycoon" variant="link">
              <Button variant="outline-secondary" disabled>
                Coming Soon
              </Button>
            </NavLink>
          </Col>
        </Row>
      </div>
    </Col>
  );
}

export default memo(PoolTycoonWidget);
