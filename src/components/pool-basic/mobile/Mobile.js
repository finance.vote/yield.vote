import Back from "components/back";
import Spinner from "components/indicators/Spinner";
import Deposit from "../components/deposit";
import PoolDetails from "../components/details";
import HeaderStats from "../components/header-stats";
import MyPositions from "../components/my-positions";
import PoolName from "../components/pool-name";
import WithdrawForm from "../components/withdraw-form";
import mobileStyles from "./Mobile.module.scss";

const Mobile = ({
  poolInstance,
  volume,
  tvl,
  poolData,
  filled,
  precision,
  isFinished,
  getStyle,
  depositAmount,
  handleChangeDepositAmount,
  isPoolActive,
  address,
  setMaxDepositAmount,
  requiredApprove,
  canDeposit,
  handleDeposit,
  approved,
  showSpinner,
  pageLoading,
  poolType,
  activePositions,
  inActivePositions,
  withdrawalsEvents,
  addNewPendingTransaction,
  showWithdrawDialog,
  withdrawData,
  handleCloseWithdrawDialog,
  handleShowWithdrawDialog
}) => {
  const depositComponentProps = {
    depositAmount,
    handleChangeDepositAmount,
    isPoolActive,
    address,
    poolInstance,
    setMaxDepositAmount,
    requiredApprove,
    canDeposit,
    handleDeposit,
    approved,
    showSpinner
  };

  return (
    <div className={mobileStyles.container}>
      {(!poolData.data || pageLoading || poolData.isFetching) && <Spinner overlay={true} />}

      {showWithdrawDialog && <Back onClick={handleCloseWithdrawDialog} />}

      {!showWithdrawDialog && (
        <>
          <PoolName className={mobileStyles.poolName} poolName={poolInstance.name} />
          <HeaderStats
            className={mobileStyles.headerStats}
            poolInstance={poolInstance}
            tvl={tvl}
            poolData={poolData}
            filled={filled}
            precision={precision}
          />
          <div className={mobileStyles.detailsContainer}>
            <PoolDetails
              volume={volume}
              poolInstance={poolInstance}
              poolData={poolData}
              isFinished={isFinished}
              getStyle={getStyle}
              precision={precision}
            />
          </div>

          <div className={mobileStyles.depositContainer}>
            <Deposit {...depositComponentProps} />
          </div>
          <div className={mobileStyles.myPositions}>
            <MyPositions
              activePositions={activePositions}
              inActivePositions={inActivePositions}
              withdrawalsEvents={withdrawalsEvents}
              symbols={poolInstance.symbols}
              isFinished={isFinished}
              handleShowWithdrawDialog={handleShowWithdrawDialog}
            />
          </div>
        </>
      )}
      {showWithdrawDialog && (
        <WithdrawForm
          poolType={poolType}
          value={poolInstance.value}
          position={withdrawData}
          closeModal={handleCloseWithdrawDialog}
          symbols={poolInstance.symbols}
          addNewPendingTransaction={addNewPendingTransaction}
        />
      )}
    </div>
  );
};

export { Mobile };
