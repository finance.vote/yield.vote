import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";
import PositionRow from "./position-row";

const CurrentPositions = ({
  positions,
  handleShowWithdrawDialog,
  symbols,
  isMobile,
  isHolders = false,
  isPoolFinished = false
}) => {
  const isAdditionalRewards =
    Object.keys(positions).length > 0
      ? Object.values(positions).every((position) => position.additionalRewards > 0)
      : false;

  return (
    <Row>
      <Col>
        <div className="occupied-lots__table-container">
          <Table responsive>
            <thead>
              <tr>
                {!isMobile && <th>Transaction</th>}
                <th>Deposit</th>
                {!isMobile && <th>Current Yield</th>}
                {!isMobile && !isHolders && isAdditionalRewards && <th>Additional rewards</th>}
                <th>Yield At Maturity</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {positions &&
                Object.values(positions).map((x) => (
                  <PositionRow
                    withdraw={handleShowWithdrawDialog}
                    position={x}
                    key={x.txHash}
                    symbols={symbols}
                    isHolders={isHolders}
                    isPoolFinished={isPoolFinished}
                    isAdditionalRewards={isAdditionalRewards}
                  />
                ))}
            </tbody>
          </Table>
        </div>
      </Col>
    </Row>
  );
};

export default CurrentPositions;
