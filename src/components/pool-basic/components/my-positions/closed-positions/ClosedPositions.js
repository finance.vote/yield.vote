import Spinner from "components/indicators/Spinner";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";
import ClosedPositionRow from "./closed-position-row";

const ClosedPositions = ({ inActivePositions, withdrawalsEvents, symbols }) => {
  return (
    <Row>
      <Col>
        <div className="occupied-lots__table-container">
          <Table responsive>
            <thead>
              <tr>
                <th>Deposit Transaction</th>
                <th>Deposit</th>
                <th>Yield</th>
                <th>Withdraw Transaction</th>
              </tr>
            </thead>
            <tbody>
              {!inActivePositions && (
                <>
                  <tr>
                    <td colSpan="5">
                      <Spinner />
                    </td>
                  </tr>
                </>
              )}
              {inActivePositions &&
                Object.values(inActivePositions).map((x) => (
                  <ClosedPositionRow
                    position={x}
                    key={x.txHash}
                    withdrawal={withdrawalsEvents[x.id]}
                    symbols={symbols}
                  />
                ))}
            </tbody>
          </Table>
        </div>
      </Col>
    </Row>
  );
};

export default ClosedPositions;
