import { formatNumber } from "utils/formats";
import { memo } from "react";

const HeaderStats = ({ poolInstance, tvl, poolData, filled, precision, className }) => {
  return (
    <div className={className}>
      <div>
        TVL:
        <strong>{` ${formatNumber(tvl)} ${poolInstance.symbols?.depositTokenSymbol}`}</strong>
      </div>
      <div>
        APY (Literally): <strong>{formatNumber(poolData.data?.literalAPY, precision)}%</strong>
      </div>
      <div>
        Filled: <strong>{formatNumber(filled, precision)}%</strong>
      </div>
    </div>
  );
};

export default memo(HeaderStats);
