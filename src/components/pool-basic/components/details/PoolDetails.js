const { memo } = require("react");
import { formatNumber, formatTimeToDateShort } from "utils/formats";
import styles from "./PoolDetails.module.scss";

const PoolDetails = ({ volume, poolInstance, poolData, isFinished, getStyle, precision }) => {
  return (
    <div className={styles.details}>
      <div className={styles.title}>Pool details:</div>
      <div>
        Maximum Pool Size:
        <strong>
          {` ${formatNumber(poolData.data?.maxPoolSize)}
          ${poolInstance.symbols?.depositTokenSymbol?.[0] !== "$" ? "$" : ""}${
            poolInstance.symbols?.depositTokenSymbol
          }`}
        </strong>
      </div>
      <div>
        Volume:
        <strong>
          {` ${formatNumber(volume, 3)} ${
            poolInstance.symbols?.depositTokenSymbol?.[0] !== "$" ? "$" : ""
          }${poolInstance.symbols?.depositTokenSymbol}`}
        </strong>
        {` (24hrs)`}
      </div>
      <div>
        Current Yield: <strong>{formatNumber(poolData.data?.currentYield, precision)}%</strong>
      </div>
      <div>
        Start Date:
        <strong>
          {` ${poolData.data ? formatTimeToDateShort(poolData.data?.dates.startTime) : "-"}`}
        </strong>
      </div>
      <div className={isFinished ? getStyle() : ""}>
        Maturity Date:
        <strong>{` ${
          poolData.data ? formatTimeToDateShort(poolData.data?.dates.endTime) : "-"
        }`}</strong>
      </div>
    </div>
  );
};

export default memo(PoolDetails);
