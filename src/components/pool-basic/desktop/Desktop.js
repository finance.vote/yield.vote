import Spinner from "components/indicators/Spinner";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Deposit from "../components/deposit";
import PoolDetails from "../components/details";
import HeaderStats from "../components/header-stats";
import MyPositions from "../components/my-positions";
import PoolName from "../components/pool-name";
import WithdrawForm from "../components/withdraw-form";
import desktopStyles from "./Desktop.module.scss";

const Desktop = ({
  poolData,
  pageLoading,
  poolInstance,
  tvl,
  precision,
  filled,
  volume,
  isFinished,
  getStyle,
  depositAmount,
  handleChangeDepositAmount,
  isPoolActive,
  address,
  setMaxDepositAmount,
  requiredApprove,
  canDeposit,
  handleDeposit,
  approved,
  showSpinner,
  poolType,
  activePositions,
  inActivePositions,
  withdrawalsEvents,
  addNewPendingTransaction,
  showWithdrawDialog,
  withdrawData,
  handleCloseWithdrawDialog,
  handleShowWithdrawDialog,
  isPositionsLoading
}) => {
  const depositComponentProps = {
    depositAmount,
    handleChangeDepositAmount,
    isPoolActive,
    address,
    poolInstance,
    setMaxDepositAmount,
    requiredApprove,
    canDeposit,
    handleDeposit,
    approved,
    showSpinner
  };

  return (
    <Container fluid>
      {(!poolData.data || pageLoading) && <Spinner overlay={true} />}
      <Row>
        <Col>
          <PoolName className={desktopStyles.poolName} poolName={poolInstance.name} />
        </Col>
      </Row>
      <Row className={desktopStyles.total}>
        <Col>
          <HeaderStats
            className={desktopStyles.headerStats}
            poolInstance={poolInstance}
            tvl={tvl}
            poolData={poolData}
            filled={filled}
            precision={precision}
          />
        </Col>
      </Row>
      <Row>
        <Col sm="12" md="5">
          <PoolDetails
            volume={volume}
            poolInstance={poolInstance}
            poolData={poolData}
            isFinished={isFinished}
            getStyle={getStyle}
            precision={precision}
          />
        </Col>
        <Col sm="12" md="7">
          <Deposit {...depositComponentProps} />
        </Col>
      </Row>
      {isPositionsLoading ? (
        <Spinner />
      ) : (
        <MyPositions
          activePositions={activePositions}
          inActivePositions={inActivePositions}
          withdrawalsEvents={withdrawalsEvents}
          symbols={poolInstance.symbols}
          isFinished={isFinished}
          handleShowWithdrawDialog={handleShowWithdrawDialog}
        />
      )}

      <Modal
        className="withdraw-modal"
        show={showWithdrawDialog}
        onHide={handleCloseWithdrawDialog}
        centered>
        <Modal.Body>
          <WithdrawForm
            value={poolInstance.value}
            position={withdrawData}
            closeModal={handleCloseWithdrawDialog}
            symbols={poolInstance.symbols}
            addNewPendingTransaction={addNewPendingTransaction}
            poolType={poolType}
          />
        </Modal.Body>
      </Modal>
    </Container>
  );
};

export { Desktop };
