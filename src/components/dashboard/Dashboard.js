import { Statistic } from "antd";
import Chart from "chart.js";
import SlotView from "components/slot-view";
import { useEffect } from "react";
import { getRewardGraph } from "utils/liquidity";
import { useResidentPool } from "utils/queries";
import "./Dashboard.css";

const startOptions2 = {
  type: "line",
  data: {
    //        labels: ['Label'],
    datasets: [
      {
        label: "Token Rewards For Occupying Lot",
        data: [
          { x: 1, y: 12 },
          { x: 2, y: 19 },
          { x: 3, y: 3 },
          { x: 4, y: 5 },
          { x: 5, y: 2 },
          { x: 6, y: 3 }
        ],
        backgroundColor: ["rgba(125, 226, 19, 0.3)", "rgba(0, 0, 0, 0)"],
        borderColor: ["rgba(125, 226, 19, 1)", "rgba(125, 226, 19, 0.2)"],
        borderWidth: 4
      }
    ]
  },
  options: {
    scales: {
      scaleLabel: {
        labelString: "Block Number"
      },
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ],
      xAxes: [{}]
    }
  }
};

function Dashboard() {
  console.log("dashboard.js");
  //TODO: change contract name to poolIndex or the function to get all pools
  const contractName = "LiquidityMining";
  const globals = useResidentPool(contractName);
  const slotIds = [...Array(2).keys()];

  let chart;

  const getRewardsTimeSeries = async () => {
    const ts = await getRewardGraph(contractName, 100);
    const xVals = ts.map((p) => p.x);

    chart.data.datasets[0].data = ts;
    chart.data.labels = xVals;
    chart.update();
  };

  useEffect(() => {
    const ctx = document.getElementById("rewardsChart");

    ctx.style.backgroundColor = "rgba(72, 61, 139, 1)";

    chart = new Chart(ctx, startOptions2);

    getRewardsTimeSeries();
  }, []);

  return (
    <div>
      <canvas id="rewardsChart" className="rewardsChart" />

      <Statistic title="Max Stakers" value={globals.data?.maxStakers} />
      <Statistic title="Current Stakers" value={globals.data?.numStakers} />
      <Statistic title="Minimum Deposit" value={globals.data?.minDeposit} />
      <Statistic title="Maximum Deposit" value={globals.data?.maxDeposit} />
      <Statistic title="Minimum Burn Rate" value={globals.data?.minBurnRate} />
      <Statistic title="Total Staked All Time" value={globals.data?.totalStaked} />
      <Statistic title="Total Rewards Paid All Time" value={globals.data?.totalRewards} />
      {slotIds.map((x) => (
        <SlotView contractName={contractName} slotId={x + 1} key={x} />
      ))}
    </div>
  );
}

export default Dashboard;
