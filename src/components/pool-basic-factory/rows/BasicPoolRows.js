import FinanceVoteImage from "assets/FinanceVoteLogo.svg";
import ITrustLogoImage from "assets/ITrustLogo.svg";
import Spinner from "components/indicators/Spinner";
import Table from "components/table";
import { memo, useEffect, useState } from "react";
import { formatNumber, formatTimeToDateShort } from "utils/formats";
import { getPartnersDeposit } from "utils/helper";
import { useGetPoolsAll } from "utils/queries/useAllPools";

const choosePartnerImage = (partnerName) => {
  switch (partnerName) {
    case "finance":
      return FinanceVoteImage;
    case "itrust":
      return ITrustLogoImage;
  }
};

const link = (cell, row) => {
  return (
    <a href={"/#/pool/basic/" + cell} rel="noopener noreferrer">
      {row.name}
    </a>
  );
};

function BasicPoolRows() {
  const pools = useGetPoolsAll("basic");
  const [datas, setDatas] = useState([]);
  const [loadingData, setLoadingData] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        setLoadingData(true);
        if (pools.data) {
          const { partners, deposits } = await getPartnersDeposit();

          for (let i = 0; i < pools.data?.length; i++) {
            const pool = pools.data[i];
            const key = deposits[pool.depositToken.toLowerCase()];
            if (key && key !== "") partners[key].pools.push(pool);
          }

          const data = Object.values(partners).length
            ? Object.values(partners).map((x) => {
                return {
                  ...x,
                  img: choosePartnerImage(x.key)
                };
              })
            : [];
          setDatas(data);
        }
      } catch (error) {
        console.error("Basic pool row loading fail: " + error);
      } finally {
        setLoadingData(false);
      }
    })();
  }, [pools.data]);

  const columns = [
    {
      accessor: "index",
      Header: "Index ⇅",
      Cell: ({ value, row }) => link(value, row)
    },
    {
      accessor: "basicTVL",
      Header: "TVL, FVT ⇅",
      Cell: ({ value }) => formatNumber(value, 0)
    },
    {
      accessor: "literalAPY",
      Header: "APY, % ⇅",
      Cell: ({ value }) => formatNumber(value, 0)
    },
    {
      accessor: "maxPoolSize",
      Header: "Pool Cap, FVT ⇅",
      Cell: ({ value }) => formatNumber(value)
    },
    {
      accessor: "dates.startTime",
      Header: "Start Date ⇅",
      formatter: ({ value }) => formatTimeToDateShort(value)
    },
    {
      accessor: "dates",
      Header: "Maturity Date ⇅",
      formatter: ({ value }) => formatTimeToDateShort(value.endTime)
    }
  ];

  return (
    <div className="all-pools">
      {(loadingData || pools.isLoading) && <Spinner />}
      {datas.map((x, index) => {
        return (
          <div className="all-pools__section" key={`all-pools__section-${index}`}>
            <div className="all-pools__section__header">
              <a href={x.url}>
                <img src={x.img} alt={x.name} />
              </a>
            </div>
            <Table data={x.pools} columns={columns} />
          </div>
        );
      })}
    </div>
  );
}

export default memo(BasicPoolRows);
