import "./CloseModalButton.scss";

const CloseModalButton = ({ onClick }) => {
  return <div className="closeButton" onClick={onClick} onKeyDown={onClick} aria-hidden="true" />;
};

export default CloseModalButton;
