import classnames from "classnames";
import { memo } from "react";
import styles from "./Steps.module.scss";

const Steps = ({ requiredApprove, approved }) => {
  return (
    <div className={styles.stepsContainer}>
      <div
        className={classnames(
          styles.circle,
          requiredApprove && !approved ? styles.circleFill : ""
        )}>
        1
      </div>
      <div className={classnames(styles.circle, approved ? styles.circleFill : "")}>2</div>
    </div>
  );
};

export default memo(Steps);
