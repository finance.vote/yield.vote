import classNames from "classnames";
import { Button, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { formatNumber, formatTimeToDateShort } from "utils/formats";
import styles from "./Mobile.module.scss";

const LeftSide = ({ pool, symbol }) => {
  const tvl = pool.data?.basicTVL ? pool.data.basicTVL : 0;
  const filled = (tvl * 100) / pool.data?.maxPoolSize;

  return (
    <>
      <div>
        <div>TVL</div>
        <div>
          <strong>{` ${formatNumber(pool.data?.basicTVL, 2)}${symbol}`}</strong>
        </div>
      </div>
      <div className={styles.secondPart}>
        <div>
          Pool Cap:
          <strong>
            {` ${formatNumber(pool.data?.maxPoolSize)}`}&nbsp;{`${symbol}`}
          </strong>
        </div>
        <div>
          Filled: <strong>{formatNumber(filled, 2)}%</strong>
        </div>
      </div>
    </>
  );
};

const RightSide = ({ pool }) => {
  return (
    <>
      <div className={styles.right}>
        <div>APY</div>
        <div>
          <strong>{formatNumber(pool.data?.literalAPY, 0)}% </strong>
        </div>
      </div>
      <div className={classNames(styles.secondPart, styles.right)}>
        <div>
          Start Date:
          <strong className="pool-basic-widget__start-date">
            {` ${formatTimeToDateShort(pool.data?.dates?.startTime, "/")}`}
          </strong>
        </div>
        <div>
          Maturity Date: <strong>{formatTimeToDateShort(pool.data?.dates?.endTime, "/")}</strong>
        </div>
      </div>
    </>
  );
};

const Footer = ({ className, url, styles }) => {
  return (
    <>
      <Col xs="12" className="pool-basic-widget__button">
        <NavLink to={url} variant="link">
          <Button className={styles[`${className}-widget-btn`]} variant="secondary">
            Pool Details
          </Button>
        </NavLink>
      </Col>
    </>
  );
};

export { LeftSide, RightSide, Footer };
