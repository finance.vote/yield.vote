import { Button, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { formatNumber, formatTimeToDateShort } from "utils/formats";

const LeftSide = ({ pool, symbol }) => {
  return (
    <>
      <div>TVL</div>
      <div>
        <strong>
          {formatNumber(pool.data?.basicTVL, 2)} {symbol}
        </strong>
      </div>
      <div>APY</div>
      <div>
        <strong>{formatNumber(pool.data?.literalAPY, 0)}% </strong>
      </div>
    </>
  );
};

const RightSide = ({ pool, symbol }) => {
  return (
    <>
      <div>Pool Cap</div>
      <div>
        <strong>
          {formatNumber(pool.data?.maxPoolSize)} {symbol}
        </strong>
      </div>
      <div>Start Date</div>
      <div>
        <strong className="pool-basic-widget__start-date">
          {formatTimeToDateShort(pool.data?.dates?.startTime)}
        </strong>
      </div>
      <div>Maturity Date</div>
      <div>
        <strong>{formatTimeToDateShort(pool.data?.dates?.endTime)}</strong>
      </div>
    </>
  );
};

const Footer = ({ image, className, url, styles }) => {
  return (
    <>
      <Col xs="6">
        <div className="image-container">
          {image && <img src={image} alt={`${className}-pool`} />}
        </div>
      </Col>
      <Col xs="6" className="pool-basic-widget__button">
        <NavLink to={url} variant="link">
          <Button className={styles[`${className}-widget-btn`]} variant="secondary">
            Pool Details
          </Button>
        </NavLink>
      </Col>
    </>
  );
};

export { Footer, LeftSide, RightSide };
