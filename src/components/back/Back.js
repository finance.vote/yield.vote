import { memo } from "react";
import styles from "./Back.module.scss";

const Back = ({ onClick }) => {
  return (
    <div className={styles.back} onClick={onClick}>
      Back
    </div>
  );
};

export default memo(Back);
