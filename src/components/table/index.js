import { useSortBy, useTable } from "react-table";

const Table = ({ data, columns }) => {
  const { getTableProps, getTableBodyProps, rows, prepareRow, headers } = useTable(
    {
      columns,
      data
    },
    useSortBy
  );

  return (
    <table className="table" {...getTableProps()}>
      <thead>
        <tr>
          {headers.map((column, idx) => {
            return (
              <th
                key={idx}
                aria-label={column.isSorted ? "desc" : ""}
                {...column.getHeaderProps(column.getSortByToggleProps())}>
                {column.render("Header")}
              </th>
            );
          })}
        </tr>
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, index) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()} key={index}>
              {row.cells.map((cell, idx) => {
                return (
                  <td
                    key={idx}
                    className={`occupied-lots__${cell.column.id}`}
                    {...cell.getCellProps()}>
                    {cell.render("Cell")}
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Table;
