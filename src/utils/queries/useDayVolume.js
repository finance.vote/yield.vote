import { useQuery } from "react-query";
import { getPoolInstance } from "utils/helper";
import { BASIC_CONTRACT_TYPE, get24hrsVolume } from "utils/poolHelper";
import { getResident24hrsVolume } from "utils/residentPool";

const DAY_VOLUMES = {
  resident: getResident24hrsVolume,
  [BASIC_CONTRACT_TYPE.BASIC]: get24hrsVolume,
  [BASIC_CONTRACT_TYPE.PERMISSIONLESS]: get24hrsVolume
};

export const use24HrsVolume = (contractType, poolIndex, chainId, positions) =>
  useQuery(
    ["volume", contractType, poolIndex, chainId, positions?.length],
    async () => {
      return DAY_VOLUMES[contractType](getPoolInstance(contractType), poolIndex, positions);
    },
    {
      enabled: !!(poolIndex && chainId),
      initialData: 0
    }
  );
