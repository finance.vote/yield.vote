import { useQuery } from "react-query";
import { getBasicAccountBalance } from "utils/basicPool";
import { getPermissionlessBasicAccountBalance } from "utils/permissionlessBasicPool";
import { BASIC_CONTRACT_TYPE } from "utils/poolHelper";
import { getResidentLPBalance } from "utils/residentPool";

const ACCOUNT_BALANCE = {
  [BASIC_CONTRACT_TYPE.BASIC]: getBasicAccountBalance,
  [BASIC_CONTRACT_TYPE.PERMISSIONLESS]: getPermissionlessBasicAccountBalance,
  resident: getResidentLPBalance
};

export const useFactoryAccountBalance = (contractType, poolIndex, chainId) =>
  useQuery(
    ["balance", contractType, poolIndex, chainId],
    async () => ACCOUNT_BALANCE[contractType](poolIndex, chainId),
    {
      enabled: !!(poolIndex || chainId)
    }
  );
