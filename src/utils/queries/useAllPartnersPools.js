import { useQuery } from "react-query";
import { getAllPartnersPools } from "utils/helper";

export const useAllPartnersPools = (light, currentNetwork) =>
  useQuery(["allPartnersPools", light, currentNetwork], async () => getAllPartnersPools(light), {
    enabled: !!currentNetwork
  });
