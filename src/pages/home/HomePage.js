import FinanceVoteImage from "assets/FinanceVoteLogo.svg";
import { If } from "components/If";
import Spinner from "components/indicators/Spinner";
import PoolBasicWidget from "components/pool-basic-widget";
import PoolResidentWidget from "components/pool-resident-widget";
import PoolTycoonWidget from "components/pool-tycoon-widget";
import { MainContext } from "context/context";
import jss from "jss";
import preset from "jss-preset-default";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { BASIC_CONTRACT_TYPE, widgetImages } from "utils/poolHelper";
import { useAllPartnersPools } from "utils/queries/useAllPartnersPools";
import poolsPerChain from "../../poolsPerChain.json";
import "./HomePage.scss";

jss.setup(preset());

const tokenLinks = {
  //  1: 'https://app.uniswap.org/#/swap?use=V2&outputCurrency=0x45080a6531d671DDFf20DB42f93792a489685e32&inputCurrency=ETH',
  1: "https://app.sushi.com/swap?outputCurrency=0x45080a6531d671DDFf20DB42f93792a489685e32&inputCurrency=ETH",
  3: "https://app.sushi.com/swap?outputCurrency=0xF7eF90B602F1332d0c12cd8Da6cd25130c768929&inputCurrency=ETH",
  333: ""
};

const lpLinks = {
  //  1: 'https://app.uniswap.org/#/add/v2/ETH/0x45080a6531d671DDFf20DB42f93792a489685e32',
  1: "https://app.sushi.com/add/ETH/0x45080a6531d671DDFf20DB42f93792a489685e32",
  3: "https://app.sushi.com/add/ETH/0xF7eF90B602F1332d0c12cd8Da6cd25130c768929"
};

const createStyle = (partner, theme) => {
  const sheet = jss
    .createStyleSheet({
      [partner + `-widget-container`]: {
        border: `1px solid ${theme.widget.border}` ?? "#000"
      },
      [partner + `-widget-title`]: {
        color: theme.widget.color ?? "#000"
      },
      [partner + `-widget-btn`]: {
        backgroundColor: `${theme.widget.color} !important` ?? "#000",
        "&:hover": {
          backgroundColor: "transparent !important",
          color: `${theme.widget.color} !important` ?? "#000",
          borderColor: `${theme.widget.color} !important` ?? "#000"
        },
        "&:disabled": {
          backgroundColor: "transparent !important",
          border: `1px solid #b8e288 !important`,
          color: `#b8e288 !important`
        }
      }
    })
    .attach();

  return sheet.classes;
};

function HomePage() {
  const { currentNetwork } = useContext(MainContext);
  const { data: partners, isLoading } = useAllPartnersPools(true, currentNetwork);
  const [finance, setFinance] = useState();

  useEffect(() => {
    if (partners) {
      const finance = partners.find((x) => x.key === "finance");
      setFinance({
        ...finance,
        styles: createStyle(finance?.key, finance.theme)
      });
    }
  }, [partners]);

  return (
    <Container className="home-page">
      <Row>
        <Col md={10} sm={12}>
          <div className="home-page__header">A competitive yield environment</div>
          <div className="home-page__description">
            These staking pools provide users with yield opportunities. Each pool offers a unique
            mechanism for trustlessly distributing tokens to stakers. Users earn ‘Literally APY’ in
            basic pool, or ‘APE PY’ in both the resident and tycoon pools, by claiming and holding
            protocol property.
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="home-page__partner-logo">
            <img src={FinanceVoteImage} alt="finance.vote" />
          </div>
          <div className="home-page__actions">
            <div className="home-page__action">
              <a
                href={currentNetwork ? tokenLinks[currentNetwork] : ""}
                target="_blank"
                rel="noreferrer">
                <Button variant="outline-secondary">Buy FVT Tokens</Button>
              </a>
            </div>
            <div className="home-page__action mb-3">
              <a
                href={currentNetwork ? lpLinks[currentNetwork] : ""}
                target="_blank"
                rel="noreferrer">
                <Button variant="outline-secondary">Provide Liquidity for FVT</Button>
              </a>
            </div>
          </div>
        </Col>
      </Row>
      <Row className="home-page__pools">
        {!finance || !finance.styles || isLoading ? (
          <Spinner />
        ) : (
          <>
            <If condition={finance.basicPools.length}>
              {finance.basicPools.map((basic) => {
                if (poolsPerChain["finance"][currentNetwork].basic.includes(basic.index)) {
                  return (
                    <PoolBasicWidget
                      key={`finance-basic-${basic.index}`}
                      contractType={BASIC_CONTRACT_TYPE.BASIC}
                      className={finance.key}
                      styles={finance.styles}
                      poolIndex={basic.index}
                      url={`/pool/basic/${basic.index}`}
                    />
                  );
                }
              })}
            </If>

            <If condition={finance.permissionlessBasicPools.length}>
              {finance.permissionlessBasicPools.map((basic) => {
                if (
                  poolsPerChain["finance"][currentNetwork].permissionlessBasic.includes(basic.index)
                ) {
                  return (
                    <PoolBasicWidget
                      key={`finance-permissionlessBasic-${basic.index}`}
                      contractType={BASIC_CONTRACT_TYPE.PERMISSIONLESS}
                      className={finance.key}
                      styles={finance.styles}
                      poolIndex={basic.index}
                      url={`/pool/permissionless-basic/${basic.index}`}
                    />
                  );
                }
              })}
            </If>

            <If condition={finance.residentPools.length}>
              {finance.residentPools.map((resident) => {
                if (poolsPerChain["finance"][currentNetwork].resident.includes(resident.index)) {
                  return (
                    <PoolResidentWidget
                      key={`finance-resident-${resident.index}`}
                      image={widgetImages.Buildings}
                      className={finance.key}
                      styles={finance.styles}
                      index={resident.index}
                    />
                  );
                }
              })}
            </If>

            {/* 
            Uncomment when tycoon pool will be added
            <PoolResidentWidget
              image={widgetImages.Tycoon}
              className={finance.key}
              styles={finance.styles}
              index={1}
              tycoon={true}
            /> */}

            {/* Empty, temporary component to remove when tycoon poll will be added */}
            <PoolTycoonWidget image={widgetImages.Tycoon} />
          </>
        )}
      </Row>
    </Container>
  );
}

HomePage.propTypes = {
  className: PropTypes.string
};

export default memo(HomePage);
