import { memo } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Page from "pages/Page";
import "./HowToPlay.scss";

function HowToPlay({ className }) {
  return <Page className={classnames("home-page", className)} />;
}

HowToPlay.propTypes = {
  className: PropTypes.string
};

export default memo(HowToPlay);
