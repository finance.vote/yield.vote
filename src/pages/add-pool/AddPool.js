import { ethInstance } from "evm-chain-scripts";
import PropTypes from "prop-types";
import { memo, useState } from "react";
import Container from "react-bootstrap/Container";
import ReactSelect from "react-select";
import "./AddPool.scss";
import BasicPoolForm from "./basic-pool-form";
import LiquidityPoolForm from "./liquidity-pool-form";

const options = [
  { value: "liquidityFactory", label: "LiquidityFactory" },
  { value: "basicPoolFactory", label: "BasicPoolFactory" },
  { value: "permissionlessBasicPoolFactory", label: "PermissionlessBasicPoolFactory" }
];

const AddPool = () => {
  const [selectedFactory, setSelectedFactory] = useState({
    value: "liquidityFactory",
    label: "LiquidityFactory"
  });

  const handleSelectChange = (item) => {
    setSelectedFactory(item);
  };

  const checkTokenAddress = async (address) => {
    try {
      const result = await ethInstance.readWeb3().getCode(address);
      return result;
    } catch {
      return "0x";
    }
  };

  return (
    <div className="factory-form-container">
      <Container>
        <h1>Add pool</h1>
        <div className="form-header">
          <div className="label">Select factory</div>
          <ReactSelect
            className="select"
            classNamePrefix="select"
            options={options}
            onChange={handleSelectChange}
            value={selectedFactory}
          />
        </div>
        <div className="form-details">
          {selectedFactory.value === "liquidityFactory" && (
            <LiquidityPoolForm checkTokenAddress={checkTokenAddress} />
          )}
          {selectedFactory.value === "basicPoolFactory" && (
            <BasicPoolForm checkTokenAddress={checkTokenAddress} />
          )}
          {selectedFactory.value === "permissionlessBasicPoolFactory" && (
            <BasicPoolForm checkTokenAddress={checkTokenAddress} permissionless={true} />
          )}
        </div>
      </Container>
    </div>
  );
};

AddPool.propTypes = {
  className: PropTypes.string
};

export default memo(AddPool);
