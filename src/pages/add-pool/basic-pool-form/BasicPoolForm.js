import Spinner from "components/indicators/Spinner";
import { ethInstance, toBN } from "evm-chain-scripts";
import { Form, Formik } from "formik";
import PropTypes from "prop-types";
import { memo, useState } from "react";
import Button from "react-bootstrap/Button";
import { toast } from "react-toastify";
import { addBasicPool } from "utils/basicPool";
import { inputToArray } from "utils/helper";
import { addPermissionlessBasicPool } from "utils/permissionlessBasicPool";
import { TRANSACTION_STATUS, contracts } from "utils/poolHelper";
import * as Yup from "yup";
import "../AddPool.scss";
import FormInput from "../form-input";

const requiredText = "Field is required";
const positiveText = "Value must be a positive number";
const notAddressText = "It is not a token address";
const notWalletAddress = "It is not a wallet address";

function AddBasicPoolForm({ checkTokenAddress, permissionless = false }) {
  const [isLoading, setIsLoading] = useState(false);

  const validateArray = async (value, isAddress = false) => {
    const array = value ? inputToArray(value) : [];
    if (isAddress) {
      const addresses = await Promise.all(array.map((item) => checkTokenAddress(item)));
      return !addresses.includes("0x");
    } else {
      return array;
    }
  };

  const validateWalletAddress = async (address) => {
    try {
      return (await ethInstance.readWeb3().getCode(address)) === "0x";
    } catch {
      return false;
    }
  };

  const inputsValidation = Yup.object({
    startTime: Yup.number().min(0).required(requiredText),
    maxDeposit: Yup.string()
      .required(requiredText)
      .test("maxDeposit", "It is not a WEI value", (value) => {
        const onlyDigits = /^[0-9]+$/.test(value);
        return onlyDigits;
      }),
    rewardsWeiPerSecondPerToken: Yup.string()
      .required(requiredText)
      .test("rewardsWeiPerSecondPerToken", "It is not a WEI value", (value) => {
        const onlyDigits = /^[0-9]+$/.test(value);
        return onlyDigits;
      }),
    programLengthDays: Yup.number().positive(positiveText).required(requiredText),
    depositTokenAddress: Yup.string()
      .required(requiredText)
      .test(
        "isAddress",
        notAddressText,
        async (address) => (await checkTokenAddress(address)) !== "0x"
      ),
    excessBeneficiary: permissionless
      ? Yup.string()
          .required(requiredText)
          .test(
            "isWalletAddress",
            notWalletAddress,
            async (address) => await validateWalletAddress(address)
          )
      : null,
    rewardTokenAddresses:
      Yup.string()
        .required(requiredText)
        .test("isAddress", "It is not an token addresses list", async (value) => {
          try {
            return await validateArray(value, true);
          } catch (err) {
            console.log(err);
            return false;
          }
        })
        .test(
          "isSameLengthAsRewardsArray",
          "This array has different length than 'Rewards per second per token' field",
          (value, context) => {
            try {
              const addresses = inputToArray(value);
              const rewards = inputToArray(context.parent.rewardsWeiPerSecondPerToken);
              return addresses?.length === rewards?.length;
            } catch (err) {
              console.log(err);
              return false;
            }
          }
        ) || Yup.array().nullable(),
    ipfsHash: Yup.string().required(requiredText),
    name: Yup.string().required(requiredText)
  });

  const approveAddPool = async ({ depositTokenAddress, maxDeposit }) => {
    try {
      const tokenContract = await ethInstance.getWriteContractByAddress(
        contracts.Token,
        depositTokenAddress
      );

      const account = await ethInstance.getEthAccount(false);
      const contractAddress =
        contracts.PermissionlessBasicPoolFactory.networks[ethInstance.getChainId()].address;

      const currentApproval = await tokenContract.allowance(account, contractAddress);

      if (currentApproval.gte(toBN(maxDeposit))) return;

      const transaction = await tokenContract.approve(contractAddress, maxDeposit);
      const result = await transaction.wait();

      const setApprove = (result) => {
        if (result?.status === TRANSACTION_STATUS.CONFIRMED)
          toast.success(`Transaction approved!`, { position: "top-center" });
        else toast.warn(`Transaction unapproved`, { position: "top-center" });
      };

      setApprove(result);
    } catch (error) {
      console.error("Approving addPool failed: " + error);
    }
  };

  const handleSubmit = async (values) => {
    try {
      setIsLoading(true);

      if (permissionless) {
        await approveAddPool(values);
      }

      const transaction = permissionless
        ? await addPermissionlessBasicPool(values)
        : await addBasicPool(values);

      const result = await transaction.wait();

      if (result?.status === TRANSACTION_STATUS.CONFIRMED)
        toast.success(`The new pool has been added!`, {
          position: "top-center"
        });
      else
        toast.warn(`The new pool hasn't been added!`, {
          position: "top-center"
        });
    } catch (e) {
      toast.error(e, { position: "top-center" });
      console.error(e);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Formik
      initialValues={{
        startTime: "",
        maxDeposit: "",
        rewardsWeiPerSecondPerToken: "",
        programLengthDays: "",
        depositTokenAddress: "",
        excessBeneficiary: "",
        rewardTokenAddresses: "",
        ipfsHash: "",
        name: ""
      }}
      validationSchema={inputsValidation}
      onSubmit={handleSubmit}>
      {({ errors }) =>
        !isLoading ? (
          <Form>
            <FormInput
              label="Start time"
              name="startTime"
              type="number"
              placeholder="Please enter start time"
            />
            <FormInput
              label="Maximum deposit amount (in Wei)"
              name="maxDeposit"
              type="text"
              placeholder="Please enter maximum deposit amount (in Wei)"
            />
            <FormInput
              label="Rewards per second per token (in Wei)"
              name="rewardsWeiPerSecondPerToken"
              type="text"
              placeholder="Please enter rewards per second per token"
            />
            <FormInput
              label="Program length days"
              name="programLengthDays"
              type="number"
              placeholder="Please enter program length days"
            />
            <FormInput
              label="Deposit token address"
              name="depositTokenAddress"
              type="text"
              placeholder="Please enter deposit token address"
            />
            {permissionless ? (
              <FormInput
                label="Excess beneficiary address"
                name="excessBeneficiary"
                type="text"
                placeholder="Please enter excess beneficiary address"
              />
            ) : null}
            <FormInput
              label="Reward token addresses"
              name="rewardTokenAddresses"
              type="text"
              placeholder="Please enter reward token addresses"
            />
            <FormInput
              label="IPFS Hash"
              name="ipfsHash"
              type="text"
              placeholder="Please enter IPFS Hash"
            />
            <FormInput label="Name" name="name" type="text" placeholder="Please enter name" />

            <Button
              type="submit"
              variant="outline-secondary send-button"
              disabled={errors.length > 0 || isLoading}>
              {`Submit ${permissionless ? "permissionless" : ""} basic pool`}
            </Button>
          </Form>
        ) : (
          <Spinner overlay={true} />
        )
      }
    </Formik>
  );
}

AddBasicPoolForm.propTypes = {
  className: PropTypes.string
};

export default memo(AddBasicPoolForm);
