import Form from "react-bootstrap/Form";
import { useField } from "formik";
import "../AddPool.scss";

const FormInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <Form.Label>
        {label}
        <Form.Control className="form-details__value burn-rate" {...field} {...props} />
        {meta.touched && meta.error ? <div className="error-message">{meta.error}</div> : null}
      </Form.Label>
    </>
  );
};

export default FormInput;
