import { If } from "components/If";
import YieldVoteLogo from "components/logos/YieldVoteLogo";
import Menu from "components/menu";
import { MainContext } from "context/context";
import jss from "jss";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useState } from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { NavLink } from "react-router-dom";
import ErrorBoundary from "utils/ErrorBoundary";
import { useAllPartnersPools } from "utils/queries/useAllPartnersPools";
import "./Page.scss";

const loadPoolStyles = (styles) => {
  document.documentElement.style.setProperty(
    `--partner-border-light`,
    styles ? styles.borders.light : "#fff"
  );

  document.documentElement.style.setProperty(
    `--partner-border-dark`,
    styles ? styles.borders.dark : "#fff"
  );

  document.documentElement.style.setProperty(
    `--partner-base-color`,
    styles ? styles.base.color : "#fff"
  );

  document.documentElement.style.setProperty(
    `--partner-other-badge`,
    styles ? styles.other.badge : "#fff"
  );

  document.documentElement.style.setProperty(
    `--partner-menu-hover`,
    styles ? styles.menu.hover : "#fff"
  );
};

const generateUrl = (pool, type) => {
  let url = "/";

  switch (type) {
    case "basic":
      url = "/pool/basic/" + pool.index;
      break;
    case "permissionless-basic":
      url = "/pool/permissionless-basic/" + pool.index;
      break;
    case "resident":
      url = "/pool/resident/" + pool.index;
      break;
  }
  return url;
};

const generateOption = (pools, theme, type) => {
  return pools?.map((pool) => {
    const url = generateUrl(pool, type);
    return {
      label: pool.name,
      value: url,
      color: theme.base.color,
      background: theme.menu.hover,
      borderColor: theme.base.color,
      url
    };
  });
};

const getMobileMenuOptions = (partners) => {
  const options = partners?.map((partner) => {
    return {
      label: partner.name,
      options: [
        ...generateOption(partner.basicPools, partner.theme, "basic"),
        ...generateOption(partner.permissionlessBasicPools, partner.theme, "permissionless-basic"),
        ...generateOption(partner.residentPools, partner.theme, "resident")
      ]
    };
  });
  return options;
};

const MenuItems = ({ partners, closeMenu }) => {
  const createStyle = (partner, theme) => {
    const sheet = jss
      .createStyleSheet({
        [partner + `-item-menu`]: {
          "&:hover": {
            backgroundColor: theme.menu.hover ?? "#fff",
            color: theme.base.color ?? "#000",
            borderColor: theme.base.color ?? "#000"
          }
        }
      })
      .attach();

    return sheet.classes[partner + `-item-menu`];
  };

  return (
    <div className="page__left-content-wrapper flex-column sticky-top">
      <div className="page__left-content-title">Liquidity Pools</div>
      {partners?.map((partner, index) => (
        <div key={partner + index}>
          <If
            condition={
              partner.basicPools.length ||
              partner.permissionlessBasicPools.length ||
              partner.residentPools.length
            }>
            <div key={partner.name}>
              <p className="page__left-content-partner" style={{ color: partner.theme.base.color }}>
                {partner.name}
              </p>
              <nav className="page__left-content-pools">
                {partner.basicPools?.map((pool) => (
                  <NavLink
                    style={({ isActive }) => {
                      return {
                        color: isActive && partner.theme.base.color,
                        background: isActive && partner.theme.menu.hover,
                        borderColor: isActive && partner.theme.base.color
                      };
                    }}
                    className={
                      pool.disabled ? "disabled-link" : createStyle(partner.key, partner.theme)
                    }
                    to={"/pool/basic/" + pool.index}
                    variant="link"
                    onClick={closeMenu}
                    key={partner.name + pool.name + pool.index}>
                    {pool.name}
                  </NavLink>
                ))}
                {partner.permissionlessBasicPools?.map((pool) => (
                  <NavLink
                    style={({ isActive }) => {
                      return {
                        color: isActive && partner.theme.base.color,
                        background: isActive && partner.theme.menu.hover,
                        borderColor: isActive && partner.theme.base.color
                      };
                    }}
                    className={
                      pool.disabled ? "disabled-link" : createStyle(partner.key, partner.theme)
                    }
                    to={"/pool/permissionless-basic/" + pool.index}
                    variant="link"
                    onClick={closeMenu}
                    key={partner.name + pool.name + pool.index}>
                    {pool.name}
                  </NavLink>
                ))}
                {partner.residentPools?.map((pool) => (
                  <NavLink
                    style={({ isActive }) => {
                      return {
                        color: isActive && partner.theme.base.color,
                        background: isActive && partner.theme.menu.hover,
                        borderColor: isActive && partner.theme.base.color
                      };
                    }}
                    className={
                      pool.disabled ? "disabled-link" : createStyle(partner.key, partner.theme)
                    }
                    to={"/pool/resident/" + pool.index}
                    variant="link"
                    onClick={closeMenu}
                    key={partner.name + pool.name + pool.index}>
                    {pool.name}
                  </NavLink>
                ))}
              </nav>
            </div>
          </If>
        </div>
      ))}
      <div className="page__left-content-links mt-auto">
        <nav>
          <a href="https://www.youtube.com/watch?v=wxWCjCcyw1c" target="_blank" rel="noreferrer">
            How To Play
          </a>
          <a
            href="https://financevote.readthedocs.io/projects/yield/en/latest/"
            target="_blank"
            rel="noreferrer">
            Docs
          </a>
          <a href="https://www.finance.vote/" target="_blank" rel="noreferrer">
            finance.vote
          </a>
        </nav>
      </div>
    </div>
  );
};
function Page({ partnerStyles, fluid, poolMenu = true, rightContent, bottomContent }) {
  const { currentNetwork, isMobile, isTablet } = useContext(MainContext);

  const allPartners = useAllPartnersPools(true, currentNetwork);
  const partners = allPartners.data;
  const [openMenu, setOpenMenu] = useState(false);

  useEffect(() => {
    if (partnerStyles) {
      loadPoolStyles(partnerStyles);
    }
  }, [partnerStyles]);

  const closeMenu = () => setOpenMenu(false);

  return isMobile ? (
    <div className="mobile-container">
      {allPartners.isSuccess && <Menu options={getMobileMenuOptions(partners)} />}
      <div>{rightContent}</div>
    </div>
  ) : (
    <Container className={"page"} fluid={fluid}>
      <ErrorBoundary>
        <Row className="pool-page">
          {poolMenu && (
            <Col md={12} lg={3}>
              <div className="page__left-content">
                {isTablet ? (
                  <>
                    <div className="mobile-menu-content">
                      <div
                        className="mobile-menu-content-btn"
                        onClick={() => setOpenMenu(!openMenu)}
                        onKeyDown={() => setOpenMenu(!openMenu)}
                        aria-hidden="true">
                        &#8801; Menu
                      </div>
                    </div>
                    <div className="overlay" style={{ width: openMenu ? "100%" : "0%" }}>
                      <div className="menu-header">
                        <NavLink to="/">
                          <YieldVoteLogo />
                        </NavLink>
                        <div
                          className="closebtn"
                          onClick={() => setOpenMenu(!openMenu)}
                          onKeyDown={() => setOpenMenu(!openMenu)}
                          aria-hidden="true">
                          &times;
                        </div>
                      </div>
                      <div className="overlay-content">
                        {allPartners.isSuccess && (
                          <MenuItems partners={partners} closeMenu={closeMenu} />
                        )}
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    {allPartners.isSuccess && (
                      <MenuItems partners={partners} closeMenu={closeMenu} />
                    )}
                  </>
                )}
              </div>
            </Col>
          )}
          {rightContent && (
            <Col md={12} lg={9} className="page__right-content">
              {rightContent}
            </Col>
          )}
        </Row>
        {bottomContent && (
          <Row>
            <Col className="page__bottom-content">{bottomContent}</Col>
          </Row>
        )}
      </ErrorBoundary>
    </Container>
  );
}

Page.propTypes = {
  className: PropTypes.string,
  rightContent: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  bottomContent: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  poolMenu: PropTypes.bool,
  fluid: PropTypes.bool
};

export default memo(Page);
