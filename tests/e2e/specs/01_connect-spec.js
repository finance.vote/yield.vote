import Page from '../pages/page';

const page = new Page();

let metamaskWalletAddress;

describe('Connect tests', () => {
    before(() => {
		cy.visit('/')
	});
	context('Connect metamask wallet', () => {
		it('should show disclaimer', () => {
			page.getDisclaimerForm().should('exist');
		});
		it('disclaimer button should be disabled without acceptance', () => {
			page.getDisclaimerFormButton().should('have.attr', 'disabled');
		});
		it('should hide disclaimer after acceptance', () => {
			page.getDisclaimerFormCheckbox().click();
			page.getDisclaimerFormButton().click();
			page.getDisclaimerForm().should('not.exist');
		});
		page.getConnectButton().click();
		page.acceptMetamaskAccessRequest();
		page.getMetamaskWalletAddress().then((address) => {
			metamaskWalletAddress = address;
		});
		it(`should login with success`, () => {
            expect(metamaskWalletAddress).to.not.equal('0x0000000000000000000000000000000000000000');
		});
	});
});