/* eslint-disable cypress/no-unnecessary-waiting */
/* eslint-disable ui-testing/no-hard-wait */

export default class Page {
	getH1() {
		return cy.get('h1');
	}

	getConnectButton() {
		return cy.get('.app-header__menu button');
	}

	getDisclaimerForm() {
		return cy.get('.disclaimer-form');
	}

	getDisclaimerFormButton() {
		return cy.get('.disclaimer-form button');
	}

	getDisclaimerFormCheckbox() {
		return cy.get('#terms-accepted');
	}

	getMetamaskWalletAddress() {
		return cy.fetchMetamaskWalletAddress();
	}

	acceptMetamaskAccessRequest() {
		cy.acceptMetamaskAccess();
	}

	confirmMetamaskTransaction() {
		cy.confirmMetamaskTransaction();
	}

	waitUntilAvailableOnEtherscan(urlOrTx, alias) {
		if (!urlOrTx.includes('http')) {
			cy.getNetwork().then((network) => {
				const etherscanUrl =
					network.networkName === 'mainnet'
						? `https://etherscan.io/tx/${urlOrTx}`
						: `https://${network.networkName}.etherscan.io/tx/${urlOrTx}`;
				waitForTxSuccess(etherscanUrl, alias);
			});
		} else {
			waitForTxSuccess(urlOrTx, alias);
		}
	}
}

function waitForTxSuccess(url, alias) {
	cy.request(url).as(alias);
	cy.get(`@${alias}`).then((response) => {
		if (
			response.body.includes('This transaction has been included into Block No') ||
			response.body.includes('</i> Pending</span>')
		) {
			cy.wait(5000);
			waitForTxSuccess(url, alias);
		}
	});
}