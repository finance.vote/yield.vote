#!/usr/bin/env bash

pwd
cp ../contracts/build/contracts/Token.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/DummyLPToken.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/LiquidityMining.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/MostBasicYield.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/BasicPoolFactory.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/LiquidityFactory.json ./src/truffle/build/contracts/
