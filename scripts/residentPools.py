import json
from web3 import Web3
import os
from dotenv import load_dotenv
import pandas as pd

load_dotenv(dotenv_path='.env', verbose=True)
INFURA_API_KEY = os.getenv("INFURA_API_KEY")

# w3 = Web3(Web3.WebsocketProvider('wss://ropsten.infura.io/ws/v3/{}'.format(INFURA_API_KEY)))
w3 = Web3(Web3.WebsocketProvider('wss://mainnet.infura.io/ws/v3/{}'.format(INFURA_API_KEY)))
print('Current working directory: {}'.format(os.getcwd()))
print('INFURA_API_KEY: {}'.format(INFURA_API_KEY))
yieldContractAddress = '0x4F66f085727C3d8b72FFD2Efd0Bb93469648f945'
yieldContractAddress2 = '0xfe64d9A8Fd6565b6842d6574871D929809424280'


def getTransactionGas(row):
    print('getting transaction for {}'.format(row))
    rcpt = w3.eth.getTransactionReceipt(row['transactionHash'])
    tx = w3.eth.getTransaction(row['transactionHash'])
    weiSpent = rcpt['gasUsed'] * tx['gasPrice']
    ethSpent = w3.fromWei(weiSpent, 'ether')
    return {'gasUsed': rcpt['gasUsed'], 'gasPrice': tx['gasPrice'], 'ethSpent': ethSpent}


def getYieldDataV1():
    print('Getting yield data')
    yieldJson = json.load(open('./src/truffle/build/contracts/LiquidityMining.json', 'r'))
    yieldContract = w3.eth.contract(address=yieldContractAddress, abi=yieldJson['abi'])


    filter = yieldContract.events.SlotChangedHands.createFilter(fromBlock="0x0")
    events = filter.get_all_entries()
    processedEvents = [{**x, **x['args']} for x in events]
    d = []
    for i, event in enumerate(processedEvents):
        print('Processing event {}'.format(event))
        block = w3.eth.getBlock(event['blockNumber'])
        d.append({
            'slotId': event['slotId'],
            'deposit': event['deposit'],
            'owner': event['owner'],
            'burnRate': event['burnRate'],
            'transactionHash': event['transactionHash'],
            'timestamp': block.timestamp
        })

    df = pd.DataFrame(d)
    df2 = pd.DataFrame([getTransactionGas(row) for _, row in df.iterrows()])

    return pd.concat([df, df2], axis='columns')


def getYieldDataV2():
    print('Getting yield data')
    yieldJson = json.load(open('./src/truffle/build/contracts/LiquidityFactory.json', 'r'))
    yieldContract = w3.eth.contract(address=yieldContractAddress2, abi=yieldJson['abi'])


    filter = yieldContract.events.SlotChangedHands.createFilter(fromBlock="0x0", argument_filters={'poolId': 2})
    events = filter.get_all_entries()
    processedEvents = [{**x, **x['args']} for x in events]
    d = []
    for i, event in enumerate(processedEvents):
        print('Processing event {}'.format(event))
        block = w3.eth.getBlock(event['blockNumber'])
        d.append({
            'slotId': event['slotId'],
            'deposit': event['depositWei'],
            'owner': event['owner'],
            'burnRate': event['burnRateWei'],
            'transactionHash': event['transactionHash'],
            'timestamp': block.timestamp
        })

    df = pd.DataFrame(d)
    df2 = pd.DataFrame([getTransactionGas(row) for _, row in df.iterrows()])

    return pd.concat([df, df2], axis='columns')


def getLiabilities(poolId):
    yieldJson = json.load(open('./src/truffle/build/contracts/LiquidityFactory.json', 'r'))
    yieldContract = w3.eth.contract(address=yieldContractAddress2, abi=yieldJson['abi'])

    numSlots = 50
    rewards = []
    for slotId in range(1, numSlots + 1):
        rewards.append(yieldContract.functions.getRewards(poolId, slotId).call())

    return sum([x[0] for x in rewards]), sum([x[1] for x in rewards])

rewards = getLiabilities(2)
print(rewards)
# df = getYieldDataV2()
# df.to_csv('./resident-data.csv', index=False)

